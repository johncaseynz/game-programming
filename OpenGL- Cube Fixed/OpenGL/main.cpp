#include "GL/glew.h"
#include "SFML/Graphics.hpp"
#include "SFML/OpenGL.hpp" // SFML OpenGL include
#include <iostream>
#include <cmath>

class Cube
{
	private:
		GLfloat vertexBuffer[108] = {
			// The front face.
			-1.0f, 1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,
			1.0f, 1.0f, -1.0f, -1.0f,
			1.0f,
			1.0f,
			-1.0f,
			1.0f,

			// The left face.
			1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f, 1.0f,
			1.0f,
			-1.0f,
			-1.0f,

			// The back face.
			1.0f, 1.0f, -1.0f, 1.0f, -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f,
			1.0f, -1.0f, 1.0f, -1.0f, -1.0f, -1.0f,
			-1.0f,
			-1.0f,

			// The right face.
			-1.0f, 1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f, 1.0f, 1.0f, -1.0f,
			1.0f, 1.0f, -1.0f, -1.0f, -1.0f, -1.0f, -1.0f,
			1.0f,

			// The top face.
			-1.0f, 1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, -1.0f, 1.0f,
			1.0f, -1.0f, -1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f,

			// The bottom face.
			-1.0f, -1.0f, 1.0f, -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, 1.0f, 1.0f,
			-1.0f, 1.0f, -1.0f, -1.0f, -1.0f, 1.0f, -1.0f, -1.0f
		};
		GLbyte facesIndex[36] = { 0, 1, 2, 2, 1, 5, 6, 7, 8, 8, 7, 11, 12, 13, 14, 14, 13, 17, 18, 19, 20, 20, 19, 23, 24, 25, 26, 26, 25, 29, 30, 31, 32, 32, 31, 35 };
		GLfloat textureBuffer[144] = {
			// The front face.
			0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f,
			0.0f,
			0.0f,
			1.0f,
			1.0f,
			1.0f,

			// The left face.
			0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f,
			0.0f,
			1.0f,
			1.0f,
			1.0f,

			// The back face.
			0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f,
			1.0f,
			1.0f,
			1.0f,

			// The right face.
			0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,
			1.0f,
			1.0f,

			// The top face.
			0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
			1.0f,

			// The bottom face.
			0.0f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,
			1.0f
		};


	public:
		Cube()
		{
			// load file using SFML
			sf::Image texture;
			texture.loadFromFile("glass.bmp");

			GLuint texture_handle;
			glGenTextures(1, &texture_handle); // allocate a texture handle within OpenGL
			glBindTexture(GL_TEXTURE_2D, texture_handle); // bind to the texture handle target

														  // import texture into OpenGL
			glTexImage2D(
				GL_TEXTURE_2D, 0, GL_RGBA,
				texture.getSize().x, texture.getSize().y,
				0,
				GL_RGBA, GL_UNSIGNED_BYTE, texture.getPixelsPtr()
			);

			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		}
		void render()
		{
			glEnable(GL_TEXTURE_2D);

			glEnable(GL_CULL_FACE); // enable back face culling so we don't see the hidden vertices / textures of the cube
			glCullFace(GL_BACK);
			glFrontFace(GL_CCW);

			glEnableClientState(GL_VERTEX_ARRAY);
			glEnableClientState(GL_TEXTURE_COORD_ARRAY);

			glVertexPointer(3, GL_FLOAT, 0, vertexBuffer);
			glTexCoordPointer(2, GL_FLOAT, 0, textureBuffer);

			glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_BYTE, facesIndex);
			
			glDisableClientState(GL_VERTEX_ARRAY);
			glDisableClientState(GL_TEXTURE_COORD_ARRAY);
		}
};

int main()
{
	bool texturesOn = false;
	bool wireframe = false;

	sf::ContextSettings settings;
	settings.depthBits = 24;          // Request a 24-bit depth buffer
	settings.stencilBits = 8;           // Request a 8 bits stencil buffer
	settings.antialiasingLevel = 2; // Request 2 levels of antialiasing

	// Use SFML to handle the window for us
	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML OpenGL Triangle", sf::Style::Close, settings);

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // nicest perspective correction calculations

	//prepare OpenGL surface for HSR
	//glClearDepth(1.f); // clear the z-buffer/depth buffer completely +1.0f is the furtherest away
	glClearColor(0.3f, 0.3f, 0.3f, 0.f); // set the background colour for when we clear the screen RGBA values in the 0.0 to 1.0 range. This gives us a nice grey background.



	// Setup a perspective projection & Camera position

	// GL_PROJECTION what we actually see
	glMatrixMode(GL_PROJECTION); // Select the builtin projection matrix
	glLoadIdentity();  // reset the projection matrix by loading the projection identity matrix

	GLdouble fovY = 90;
	GLdouble aspect = 1.0f;
	GLdouble zNear = 1.0f; 
	GLdouble zFar = 300.0f;

	const GLdouble pi = 3.1415926535897932384626433832795;
	GLdouble fW, fH;

	fH = tan(fovY / 360 * pi) * zNear;
	fW = fH * aspect;

	float angle = 0;

	// define a perspective projection
	glFrustum(-fW, fW, -fH, fH, zNear, zFar); // multiply the set matrix; by a perspective matrix

	Cube cube;

// Start game loop
	while (window.isOpen())
	{
		// Process events
		sf::Event Event;
		while (window.pollEvent(Event))
		{
			// Close window : exit
			if (Event.type == sf::Event::Closed)
				window.close();
		}

		//Prepare for drawing
		// Clear color and depth buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear z-buffer and set previously selected colour

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity(); // reset

		

		
		glTranslatef(0, 0,-3);		
		glRotatef(angle += 0.01f, 0, 1, 0);
		glRotatef(angle += 0.01f, 1, 0, 0);

		cube.render();

		window.display();
	}

	return EXIT_SUCCESS;
}