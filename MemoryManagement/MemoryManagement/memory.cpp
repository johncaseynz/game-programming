#include <iostream>

void countByValue(int data,int max)
{
	for (int i = 0; i < max; i++)
	{
		data++;
	}
}

void countByReference(int &data, int max)
{
	for (int i = 0; i < max; i++)
	{
		data++;
	}
}

int main(int *argv, int argc)
{
	int arrayName[8] = { 1,2,3,4,5,6,7,8 };

	int* first = arrayName; // an array is already a pointer

	for (int i = 0; i < 8; i++)
	{
		std::cout << *(first + i) << " ";
	}	
}
