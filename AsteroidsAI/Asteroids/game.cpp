#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <random>

#include "Particle.h"
#include "SpaceShip.h"
#include "Enemy.h"
#include "VectorMath.h"

bool right;
bool left;
bool rocket;

// process the state machine to turn on and off the different rocket thrusters 
void processEvents(Particle &particles, const sf::FloatRect &rect, sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		{
			left = true;
			right = false;
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		{
			left = false;
			right = true;
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		{
			left = false;
			right = false;
			rocket = true;
		}
		else if (event.type == sf::Event::KeyReleased)
		{
			left = false;
			right = false;
			rocket = false;
		}
	}
}

int main()
{
	sf::Time TimePerFrame = sf::seconds(1.f / 60); // update the screen in fixed time steps 60 times per second

	sf::ContextSettings context;
	context.antialiasingLevel = 10;

	sf::RenderWindow window(sf::VideoMode(1024, 768), "SFML Asteroids", sf::Style::Default, context);
	window.setFramerateLimit(60);


	const sf::FloatRect rect = sf::FloatRect(sf::Vector2f(0, 0), sf::Vector2f(window.getSize()));

	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;

	SpaceShip player = SpaceShip(rect, sf::Vector2f(window.getSize().x / 2, window.getSize().y / 2), 1);
	Enemy enemy = Enemy(rect, sf::Vector2f(20, 20), 1);

	while (window.isOpen())
	{
		

		processEvents(player, rect, window);
		if (left)
		{
			player.rotateLeft();
		}
		else if (right)
		{
			player.rotateRight();
		}
		else if (rocket)
		{
			player.fireRocket();
		}
		else
		{
			player.stop();
		}

		timeSinceLastUpdate += clock.restart();

		while (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;
			processEvents(player, rect, window);

			player.update(TimePerFrame);
			player.checkEdges();

			enemy.track(player);
			enemy.update(TimePerFrame);
			enemy.checkEdges();
		}

		window.clear();
		player.draw(&window);
		enemy.draw(&window);
		window.display();
	}
}

