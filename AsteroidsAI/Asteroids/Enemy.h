#pragma once
#include "SpaceShip.h"
class Enemy :
	public SpaceShip
{
public:
	Enemy();
	Enemy(const sf::FloatRect & boundary, const sf::Vector2f & location, const float & mass);
	~Enemy();

	void track(SpaceShip &player);
};

