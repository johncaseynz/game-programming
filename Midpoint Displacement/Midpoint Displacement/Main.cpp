#include <SFML/Graphics.hpp>
#include <Thor/Shapes/ConcaveShape.hpp>
#include <cmath>
#include <random>
#include <algorithm>
#include <iostream>


std::default_random_engine generator;
std::normal_distribution<double> normal(0.0, 1.0);

void midPoint(int leftIndex, int rightIndex, float heightmap[], int level, int goal, float displacement)
{
	int pivot_point = (leftIndex + rightIndex) / 2;

	float left_point = heightmap[leftIndex];
	float right_point = heightmap[rightIndex];

	float mid_point = (left_point + right_point) / 2;

	heightmap[pivot_point] = mid_point + normal(generator)*displacement;

	if (level < (goal - 1))
	{
		float displacement_reduction = displacement / 2.0f; // reduce the power of the displacement by 0.5 every iteration

		midPoint(leftIndex, pivot_point, heightmap, level +1, goal, displacement_reduction);
		midPoint(pivot_point, rightIndex, heightmap, level +1, goal, displacement_reduction);
	}
}

float* createTerrain(int* slots, int iterations, float initialHeight, float displacement)
{
	*slots = pow(2, iterations) + 1; // we want 2^n + 1 slots so that we can find the middle

	std::cout << "slots:" << slots << std::endl;

	float* heightmap = new float[*slots];

	heightmap[0] = initialHeight; // left hand side
	heightmap[*slots - 1] = initialHeight; // right hand side

	midPoint(0 /* left */, *slots - 1 /* right */, heightmap, 0, iterations, displacement);

	return heightmap;
}

int main()
{
	float WIDTH = 1024;
	float HEIGHT = 768;

	int background_slots;
	float* background = createTerrain(&background_slots,6, (HEIGHT / 2) - 100,200);

	int foreground_slots;
	float* foreground = createTerrain(&foreground_slots,4, (HEIGHT / 2)+100,50);

	sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "SFML Midpoint Displacement");
	window.setFramerateLimit(15);

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
				window.close();
		}

		window.clear();


		for (int i = 0; i < background_slots; i++)
		{
			background[i] = background[(i + 1) % background_slots];
		}

		thor::ConcaveShape backgroundShape;
		backgroundShape.setFillColor(sf::Color(158, 98, 204));
		backgroundShape.setPointCount(background_slots+2);

		float slot_width = (WIDTH / (background_slots - 1));

		for (int i = 0;i<background_slots;i++)
		{
			backgroundShape.setPoint(i,sf::Vector2f(i * slot_width, background[i]));
		}
		backgroundShape.setPoint(background_slots, sf::Vector2f(WIDTH, HEIGHT));
		backgroundShape.setPoint(background_slots+1, sf::Vector2f(0, HEIGHT));

		for (int i = 0; i < foreground_slots; i++)
		{
			foreground[i] = foreground[(i + 1) % foreground_slots];
		}

		thor::ConcaveShape foregroundShape;
		foregroundShape.setFillColor(sf::Color(127, 76, 136));
		foregroundShape.setPointCount(foreground_slots + 2);

		slot_width = (WIDTH / (foreground_slots - 1));

		for (int i = 0;i<foreground_slots;i++)
		{
			foregroundShape.setPoint(i, sf::Vector2f(i * slot_width, foreground[i]));
		}
		foregroundShape.setPoint(foreground_slots, sf::Vector2f(WIDTH, HEIGHT));
		foregroundShape.setPoint(foreground_slots + 1, sf::Vector2f(0, HEIGHT));

		window.draw(backgroundShape);
		window.draw(foregroundShape);

		window.display();
	}

	return 0;
}