#include "Particle.h"
#include "Quadtree.h"
#include "VectorMath.h"


Particle::Particle(float x, float y, float radius)
{
	width = radius;
	height = radius;

	location.x = x;
	location.y = y;

	velocity.x = 2.0f;
	velocity.y = 2.0f;

	shape.setPosition(x, y);
	shape.setRadius(width);
	shape.setFillColor(sf::Color(32, 128, 255));
}

void Particle::integrate()
{
	location += velocity;

	shape.setPosition(location);
}

void Particle::checkEdges()
{
	if ((location.x > 800) || (location.x < 0)) {
		velocity.x = velocity.x * -1;
	}
	if ((location.y > 600) || (location.y < 0)) {
		velocity.y = velocity.y * -1;
	}
}

void Particle::checkCollision(Quadtree * quadtree)
{
	std::vector <Particle*> particles = quadtree->getObjectsAt(location.x, location.y);

	for (int i = 0; i < particles.size(); i++)
	{
		sf::Vector2f difference = location - particles[i]->location;

		float distance = vm::magnitude(difference);
		if (distance != 0)
		{
			if (distance < (5 + 5))
			{
				velocity = velocity * -1.0f;
			}
		}
	}
}

void Particle::draw(sf::RenderTarget &canvas) {
	canvas.draw(shape);
}