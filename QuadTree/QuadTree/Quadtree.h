#ifndef QUADTREE_H
#define QUADTREE_H

#include <vector>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/Text.hpp>

class Quadtree;
class Particle;

class Quadtree {
public:
	Quadtree(float x, float y, float width, float height, int level, int maxLevel,int capacity);

	void addParticle(Particle *object);
	std::vector<Particle*> getObjectsAt(float x, float y);
	void clear();
	void draw(sf::RenderTarget &canvas);

	int count();
	Quadtree* getParent();

private:
	float x;
	float y;
	float width;
	float height;
	int level;
	int maxLevel;

	int capacity;

	std::vector<Particle*> objects;

	Quadtree *parent;
	Quadtree *NW;
	Quadtree *NE;
	Quadtree *SW;
	Quadtree *SE;

	sf::RectangleShape shape;

	bool contains(Quadtree *child, Particle *object);
};

#endif
