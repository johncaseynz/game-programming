#include "Quadtree.h"
#include "Particle.h"
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Graphics/Font.hpp>
#include <iostream>
const int RADIUS = 5;
int main()
{
	sf::RenderWindow app(sf::VideoMode(800, 600, 32), "Quadtree");
	app.setFramerateLimit(60);

	Quadtree quadtree(0.0f, 0.0f, 800.0f, 600.0f, 0, 6, 0);

	std::vector<Particle> objects;

	while (app.isOpen()) {
		sf::Event event;
		sf::Vector2i mousePosition = sf::Mouse::getPosition(app);
		while (app.pollEvent(event)) {
			if (event.type == sf::Event::KeyPressed) {
				if (event.key.code == sf::Keyboard::Escape) {
					app.close();
				}
			}
			if (event.type == sf::Event::MouseButtonPressed) {
				objects.push_back(Particle(mousePosition.x, mousePosition.y, RADIUS));
			}
			else if (event.key.code == sf::Keyboard::A) {
				objects.push_back(Particle(mousePosition.x, mousePosition.y, RADIUS));
			}
		}
		app.clear();

		std::cout << "sz:" << objects.size() << std::endl;

		for (int n = 0; n < objects.size(); ++n)
		{
			objects[n].checkEdges();
			objects[n].integrate();
			objects[n].checkCollision(&quadtree);
			quadtree.addParticle(&objects[n]);
			objects[n].draw(app);
		}
		quadtree.draw(app);

		std::vector<Particle*> returnObjects = quadtree.getObjectsAt(mousePosition.x, mousePosition.y);
		std::cout << returnObjects.size() << std::endl;
		quadtree.clear();

		app.display();
	}

	return 0;
}
