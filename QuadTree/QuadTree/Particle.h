#ifndef PARTICLE_H
#define PARTICLE_H
#include "Quadtree.h"
#include <SFML/System/Vector2.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/CircleShape.hpp>

class Particle {
public:
	sf::Vector2f location;
	sf::Vector2f velocity;
	float width;
	float height;

	Particle(float x, float y, float radius);
	void draw(sf::RenderTarget &canvas);
	void integrate();
	void checkEdges();
	void checkCollision(Quadtree* quadtree);

private:
	sf::CircleShape	shape;
};

#endif