#include "Quadtree.h"
#include "Particle.h"
#include <iostream>
#include <sstream>

using namespace std;

Quadtree::Quadtree(float _x, float _y, float _width, float _height, int _level, int _maxLevel, int capacity) :
	x(_x),
	y(_y),
	width(_width),
	height(_height),
	level(_level),
	maxLevel(_maxLevel)
{
	shape.setPosition(x, y);
	shape.setSize(sf::Vector2f(width, height));
	shape.setFillColor(sf::Color(0, 0, 0, 0));
	shape.setOutlineThickness(1.0f);
	shape.setOutlineColor(sf::Color(64, 128, 255));

	if (level == maxLevel) {
		return;
	}

	this->capacity = capacity;

	NW = new Quadtree(x, y, width / 2.0f, height / 2.0f, level + 1, maxLevel, 4);
	NE = new Quadtree(x + width / 2.0f, y, width / 2.0f, height / 2.0f, level + 1, maxLevel, 4);
	SW = new Quadtree(x, y + height / 2.0f, width / 2.0f, height / 2.0f, level + 1, maxLevel, 4);
	SE = new Quadtree(x + width / 2.0f, y + height / 2.0f, width / 2.0f, height / 2.0f, level + 1, maxLevel, 4);
}

void Quadtree::addParticle(Particle *object) {
	if (contains(this, object) && objects.size() < capacity)
	{
		objects.push_back(object);
	}
	else
	{
		if (level == maxLevel) {
			objects.push_back(object);
			return;
		}
		if (contains(NW, object)) {
			NW->addParticle(object);
			return;
		}
		else if (contains(NE, object))
		{
			NE->addParticle(object);
			return;
		}
		else if (contains(SW, object))
		{
			SW->addParticle(object);
			return;
		}
		else if (contains(SE, object))
		{
			SE->addParticle(object);
			return;
		}
	}
}

vector<Particle*> Quadtree::getObjectsAt(float _x, float _y) {
	if (level == maxLevel) {
		return objects;
	}

	vector<Particle*> returnObjects, childReturnObjects;
	if (!objects.empty()) {
		returnObjects = objects;
	}
	if (_x > x + width / 2.0f && _x < x + width) {
		if (_y > y + height / 2.0f && _y < y + height) {
			childReturnObjects = SE->getObjectsAt(_x, _y);
			returnObjects.insert(returnObjects.end(), childReturnObjects.begin(), childReturnObjects.end());
			return returnObjects;
		}
		else if (_y > y && _y <= y + height / 2.0f) {
			childReturnObjects = NE->getObjectsAt(_x, _y);
			returnObjects.insert(returnObjects.end(), childReturnObjects.begin(), childReturnObjects.end());
			return returnObjects;
		}
	}
	else if (_x > x && _x <= x + width / 2.0f) {
		if (_y > y + height / 2.0f && _y < y + height) {
			childReturnObjects = SW->getObjectsAt(_x, _y);
			returnObjects.insert(returnObjects.end(), childReturnObjects.begin(), childReturnObjects.end());
			return returnObjects;
		}
		else if (_y > y && _y <= y + height / 2.0f) {
			childReturnObjects = NW->getObjectsAt(_x, _y);
			returnObjects.insert(returnObjects.end(), childReturnObjects.begin(), childReturnObjects.end());
			return returnObjects;
		}
	}
	return returnObjects;
}

void Quadtree::clear() {
	if (level == maxLevel) {
		objects.clear();
		return;
	}
	else {
		NW->clear();
		NE->clear();
		SW->clear();
		SE->clear();
	}
	if (!objects.empty()) {
		objects.clear();
	}
}

void Quadtree::draw(sf::RenderTarget &canvas)
{
	canvas.draw(shape);

	if (level != maxLevel) {

		if (NE->count() > 0)
		{
			NE->draw(canvas);
		}
		if (NW->count() > 0)
		{
			NW->draw(canvas);
		}
		if (SW->count() > 0)
		{
			SW->draw(canvas);
		}
		if (SE->count() > 0)
		{
			SE->draw(canvas);
		}
	}
}


int Quadtree::count()
{
	int count = objects.size();;
	if (level != maxLevel)
	{
		count += NW->count();
		count += NE->count();
		count += SE->count();
		count += SW->count();
	}
	return count;
}

Quadtree * Quadtree::getParent()
{
	return parent;
}

bool Quadtree::contains(Quadtree *child, Particle *object) {
	return	 !(object->location.x < child->x ||
		object->location.y < child->y ||
		object->location.x > child->x + child->width ||
		object->location.y > child->y + child->height ||
		object->location.x + object->width < child->x ||
		object->location.y + object->height < child->y ||
		object->location.x + object->width > child->x + child->width ||
		object->location.y + object->height > child->y + child->height);
}
