#include <SFML/Graphics.hpp>
#include <iostream>

int main()
{
	// create the window
	sf::RenderWindow window(sf::VideoMode(800, 600), "My window");
	window.setFramerateLimit(30);

	sf::Sprite sprite;

	sprite.setPosition(sf::Vector2f(window.getSize().x / 2, window.getSize().y / 2));

	sf::Texture down;
	if (!down.loadFromFile("assets/hero_down.png"))
	{
		std::cout << "Error loading resource hero_down.png" << std::endl;
	}

	sf::Texture walk_down;
	if (!walk_down.loadFromFile("assets/hero_walk_down.png"))
	{
		std::cout << "Error loading resource hero_down.png" << std::endl;
	}

	sf::Texture walk_up;
	if (!walk_up.loadFromFile("assets/hero_walk_up.png"))
	{
		std::cout << "Error loading resource hero_walk_up.png" << std::endl;
	}

	sf::Texture walk_left;
	if (!walk_left.loadFromFile("assets/hero_walk_left.png"))
	{
		std::cout << "Error loading resource hero_walk_up.png" << std::endl;
	}

	sf::Texture walk_right;
	if (!walk_right.loadFromFile("assets/hero_walk_right.png"))
	{
		std::cout << "Error loading resource hero_walk_up.png" << std::endl;
	}

	sprite.setTexture(down);
	sprite.setScale(sf::Vector2f(2.0f, 2.0f));

	int frame = 0;
	int frameMax = 8;

	bool movementDown = false;
	bool movementUp = false;
	bool movementLeft = false;
	bool movementRight = false;

	// run the program as long as the window is open
	while (window.isOpen())
	{
		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		while (window.pollEvent(event))
		{
			// "close requested" event: we close the window
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			{
				if (!movementDown)
				{
					frame = 0;
					frameMax = 8;

					movementDown = true;
					movementUp = false;
					movementLeft = false;
					movementRight = false;
				}
				
				sprite.setTexture(walk_down);
				sprite.setTextureRect(sf::IntRect(frame * 40, 0, 40, 60));

				sf::Vector2f pos = sprite.getPosition();
				pos.y = pos.y + 10;
				sprite.setPosition(pos);


			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			{
				if (!movementUp)
				{
					frame = 0;
					frameMax = 8;

					movementDown = false;
					movementUp = true;
					movementLeft = false;
					movementRight = false;
				}

				sprite.setTexture(walk_up);
				sprite.setTextureRect(sf::IntRect(frame * 40, 0, 40, 60));

				sf::Vector2f pos = sprite.getPosition();
				pos.y = pos.y - 10;
				sprite.setPosition(pos);


			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			{
				if (!movementLeft)
				{
					frame = 0;
					frameMax = 8;

					movementDown = false;
					movementUp = false;
					movementLeft = true;
					movementRight = false;
				}

				sprite.setTexture(walk_left);
				sprite.setTextureRect(sf::IntRect(frame * 40, 0, 40, 60));

				sf::Vector2f pos = sprite.getPosition();
				pos.x = pos.x - 10;
				sprite.setPosition(pos);
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			{
				if (!movementRight)
				{
					frame = 0;
					frameMax = 8;

					movementDown = false;
					movementUp = false;
					movementLeft = false;
					movementRight = true;
				}

				sprite.setTexture(walk_right);
				sprite.setTextureRect(sf::IntRect(frame * 40, 0, 40, 60));

				sf::Vector2f pos = sprite.getPosition();
				pos.x = pos.x + 10;
				sprite.setPosition(pos);
			}

			// frame = (frame + 1) % frameMax;

			frame = frame + 1;

			if (frame == frameMax)
			{
				frame = 0;
			}
		}

		// clear the window with black color
		window.clear(sf::Color::Black);

		// draw everything here...
		window.draw(sprite);

		// end the current frame
		window.display();

		std::cout << "frame:" << frame << std::endl;
	}

	return 0;
}