#include "Spring.h"

#include <iostream>

Particle Spring::getOrigin()
{
	return origin;
}

void Spring::setOrigin(const Particle &origin)
{
	this->origin = origin;
}

float Spring::getConstant()
{
	return k;
}

float Spring::getRestLength()
{
	return restLength;
}

void Spring::update(const sf::Time & deltaTime)
{
	sf::Vector2f force = particle.getLocation() - origin.getLocation();

	float displacement = vm::magnitude(force) - restLength;

	force = vm::normalize(force);
	force =  force * -1.0f * k *  displacement;

	sf::Vector2f acceleration = force / particle.getMass();

	//particle.applyForce(sf::Vector2f(0, 30.0f));
	particle.applyForce(acceleration);
	origin.applyForce(-acceleration);

	this->particle.update(deltaTime);
	this->origin.update(deltaTime);
}

Particle& Spring::getParticle()
{
	return particle;
}

void Spring::setParticle(Particle & particle)
{
	this->particle = particle;
}

Spring::Spring(Particle &origin, Particle &weight, float springLength) : origin(origin), particle(weight)
{
	//this->origin = origin;
	//this->particle = weight;
	this->restLength = springLength;

	this->k = 0.01f;
}


Spring::~Spring()
{
}
