#include "SFML/Graphics.hpp"
#include "SFML/OpenGL.hpp" // SFML OpenGL include
#include <iostream>

int main()
{
	// Use SFML to handle the window for us
	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML OpenGL Triangle");

	//prepare the OpenGL surface for drawing
	glClearColor(0.3f, 0.3f, 0.3f, 0.f); // set the background colour for when we clear the screen RGBA values in the 0.0 to 1.0 range. This gives us a nice grey background.

	// Start game loop
	while (window.isOpen())
	{
		// Process events
		sf::Event Event;
		while (window.pollEvent(Event))
		{
			// Close window : exit
			if (Event.type == sf::Event::Closed)
				window.close();

			// Escape key : exit
			if ((Event.type == sf::Event::KeyPressed) && (Event.key.code == sf::Keyboard::Escape))
				window.close();
		}

		//Prepare for drawing
		// Clear color and depth buffer
		glClear(GL_COLOR_BUFFER_BIT);

		//Draw a 2D Red Triangle
		glBegin(GL_TRIANGLES);
			// define the colour
			glColor3f(1.0f, 0.0f, 0.0f);
			
			// define the points in CCW order
			glVertex3f(-0.5f, -0.5f, 0.0f);

			glColor3f(0.0f, 1.0f, 0.0f);
			glVertex3f(0.5f, -0.5f, 0.0f);

			glColor3f(0.0f, 0.0f, 1.0f);
			glVertex3f(0.0f, 0.5f, 0.0f);
		glEnd();

		// Use the SFML window routines to display the OpenGL triangle
		window.display();
	}

	return EXIT_SUCCESS;
}