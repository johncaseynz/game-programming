#include "SFML/Graphics.hpp"
#include "SFML/OpenGL.hpp" // SFML OpenGL include
#include <iostream>

int main()
{
	// Create the main window
	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML OpenGL");

	// Create a clock for measuring time elapsed
	sf::Clock Clock;

	//prepare OpenGL surface for HSR
	glClearDepth(1.f); // clear the z-buffer/depth buffer completely +1.0f is the furtherest away
	glClearColor(0.3f, 0.3f, 0.3f, 0.f); // set the background colour for when we clear the screen RGBA values in the 0.0 to 1.0 range. This gives us a nice grey background.

	glEnable(GL_DEPTH_TEST); // use the depth buffer to remove hidden surfaces to prevent their render + calculation
	glDepthMask(GL_TRUE); // enable program to write into the depth buffer

	// Setup a perspective projection & Camera position

	// GL_PROJECTION what we actually see
	glMatrixMode(GL_PROJECTION); // Select the builtin projection matrix
	glLoadIdentity();  // reset the projection matrix by loading the projection identity matrix

	GLdouble fovY = 90;
	GLdouble aspect = 1.0f;
	GLdouble zNear = 1.0f; 
	GLdouble zFar = 300.0f;

	const GLdouble pi = 3.1415926535897932384626433832795;
	GLdouble fW, fH;

	fH = tan(fovY / 360 * pi) * zNear;
	fW = fH * aspect;

	// define a perspective projection
	glFrustum(-fW, fW, -fH, fH, zNear, zFar); // multiply the set matrix; by a perspective matrix

	bool rotate = true;
	float angle;

	// Start game loop
	while (window.isOpen())
	{
		// Process events
		sf::Event Event;
		while (window.pollEvent(Event))
		{
			// Close window : exit
			if (Event.type == sf::Event::Closed)
				window.close();

			// Escape key : exit
			if ((Event.type == sf::Event::KeyPressed) && (Event.key.code == sf::Keyboard::Escape))
				window.close();

			if ((Event.type == sf::Event::KeyPressed) && (Event.key.code == sf::Keyboard::A)) {
				rotate = !rotate;
			}

		}

		//Prepare for drawing
		// Clear color and depth buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear z-buffer and set previously selected colour

		// Apply some transformations for the cube
		// The GL_MODELVIEW is used for transforming our model
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity(); // reset
		glTranslatef(0.f, 0.f, -200.f); // position the cube model at z-position -200; ie. away from us

		if (rotate) {
			angle = Clock.getElapsedTime().asSeconds();
		}

		// rotate x,y,z by a given angle in degrees
		glRotatef(angle * 20, 1.f, 0.f, 0.f);
		glRotatef(angle * 40, 0.f, 1.f, 0.f);
		glRotatef(angle * 60, 0.f, 0.f, 1.f);

		//Draw a cube
		glBegin(GL_QUADS);//draw some squares
		
		// define the colour of the quad
		glColor3f(0, 1, 1); // GREEN + BLUE = YELLOW 
		glVertex3f(-50.f, -50.f, -50.f);
		glVertex3f(-50.f, 50.f, -50.f);
		glVertex3f(50.f, 50.f, -50.f);
		glVertex3f(50.f, -50.f, -50.f);

		glColor3f(0, 0, 1); // BLUE
		glVertex3f(-50.f, -50.f, 50.f);
		glVertex3f(-50.f, 50.f, 50.f);
		glVertex3f(50.f, 50.f, 50.f);
		glVertex3f(50.f, -50.f, 50.f);

		glColor3f(1, 0, 1); // RED + BLUE = CYAN
		glVertex3f(-50.f, -50.f, -50.f);
		glVertex3f(-50.f, 50.f, -50.f);
		glVertex3f(-50.f, 50.f, 50.f);
		glVertex3f(-50.f, -50.f, 50.f);

		glColor3f(0, 1, 0); // GREEN
		glVertex3f(50.f, -50.f, -50.f);
		glVertex3f(50.f, 50.f, -50.f);
		glVertex3f(50.f, 50.f, 50.f);
		glVertex3f(50.f, -50.f, 50.f);

		glColor3f(1, 1, 0); // RED + GREEN   
		glVertex3f(-50.f, -50.f, 50.f);
		glVertex3f(-50.f, -50.f, -50.f);
		glVertex3f(50.f, -50.f, -50.f);
		glVertex3f(50.f, -50.f, 50.f);

		glColor3f(1, 0, 0); // RED
		glVertex3f(-50.f, 50.f, 50.f);
		glVertex3f(-50.f, 50.f, -50.f);
		glVertex3f(50.f, 50.f, -50.f);
		glVertex3f(50.f, 50.f, 50.f);

		glEnd();

		// Finally, display rendered frame on screen
		window.display();
	}

	return EXIT_SUCCESS;
}