#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>
#include <SFML\Graphics\Rect.hpp>
#include "VectorMath.h"
#include <list>
#pragma once
class Particle
{
	protected:
		sf::Vector2f previous;
		sf::Vector2f current;
		
		sf::Vector2f acceleration; // the rate of change in velocity
		sf::FloatRect boundary;

		sf::CircleShape circle;

		bool anchor;
	public:
		Particle(const sf::FloatRect &boundary, const  sf::Vector2f &location);
		~Particle();

		void checkEdges();
		void update(const sf::Time &deltaTime);
		void move(float offsetX, float offsetY);
		void draw(sf::RenderWindow & window);

		sf::Vector2f getAcceleration();
		void setAcceleration(const sf::Vector2f &acceleration);

		sf::Vector2f& getCurrentPosition();

		void setAnchor(bool anchor);
};

