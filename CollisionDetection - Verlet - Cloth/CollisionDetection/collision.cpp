#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <random>
#include <list>
#include <cmath>

#include "Particle.h"
#include "VectorMath.h"
#include "Constraint.h"


const int WIDTH = 1024;
const int HEIGHT = 768;

void processEvents(sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
	}
}

int main()
{
	sf::Time TimePerFrame = sf::seconds(1.0f / 120);

	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;

	sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "SFML Verlet Collision Detection", sf::Style::Default, settings);
	
	std::list <Constraint*> constraints;
	std::list <Particle*> particles;

	const sf::FloatRect boundary = sf::FloatRect(sf::Vector2f(0, 0), sf::Vector2f(window.getSize()));

	float left = WIDTH /2.0f;
	float top = HEIGHT /2.0f;

	float height = 10;
	float width = 10;

	const int SIZE = 16;

	Particle* data[SIZE][SIZE];

	for (int i = 0; i < SIZE; i++)
	{
		for (int j = 0; j < SIZE; j++)
		{
			Particle *particle = new Particle(boundary, sf::Vector2f(left+(i*16), top+(j*16)));
			particle->setAnchor(false);
			particle->setAcceleration(sf::Vector2f(200, 40.0f));
			particles.push_back(particle);


			data[i][j] = particle;
		}
	}
	
	data[0][0]->setAnchor(true);
	data[0][SIZE/2]->setAnchor(true);
	data[0][SIZE-1]->setAnchor(true);

	for (int i = 0; i < SIZE; i++)
	{
		for (int j = 0; j < (SIZE/2); j++)
		{
			data[i][j]->setAcceleration(sf::Vector2f(200, -40));
		}
	}


	for (int i = 0; i < SIZE; i++)
	{
		for (int j = 0; j < SIZE-1; j++)
		{
			constraints.push_back(new Constraint(data[i][j], data[i][j + 1], 16));
		}
	}
	for (int i = 0; i < SIZE-1; i++)
	{
		for (int j = 0; j < SIZE; j++)
		{
			constraints.push_back(new Constraint(data[i][j], data[i+1][j], 16));
		}
	}

	

	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;
	while (window.isOpen())
	{
		processEvents(window);

		timeSinceLastUpdate += clock.restart();

		while (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;
			processEvents(window);

			for(std::list<Particle*>::iterator it = particles.begin(); it != particles.end(); ++it)
			{
				(*it)->checkEdges();
				(*it)->update(TimePerFrame);
			}

			for (std::list<Constraint*>::iterator it = constraints.begin(); it != constraints.end(); ++it)
			{
				(*it)->update();
			}
		}
		window.clear();

		// draw the constraints
		for (std::list<Constraint*>::iterator it = constraints.begin(); it != constraints.end(); ++it)
		{
			(*it)->draw(window);
		}

		// draw particles
		for (std::list<Particle*>::iterator it = particles.begin(); it != particles.end(); ++it)
		{
			(*it)->draw(window);
		}

		window.display();
	}
}

