#include <iostream>

#include <random>
#include "stdafx.h"

std::default_random_engine generator;
std::uniform_int_distribution<int> distribution(0, 100);

const int MAX_QUEUE = 10;

void insertHead(int data[MAX_QUEUE], int &tailIndex, int value)
{
	data[0] = value;
	tailIndex = tailIndex + 1;

	// make sure we don't exceed the bounds of our array
	if (tailIndex > MAX_QUEUE - 1)
	{
		tailIndex = MAX_QUEUE - 1;
	}
}

void printData(int data[MAX_QUEUE])
{
	for (int i = 0; i < MAX_QUEUE; i++)
	{
		std::cout << data[i] << ",";
	}
	std::cout << std::endl;
}

void shuffleDown(int data[MAX_QUEUE], int tailIndex)
{
	int start = tailIndex + 1;

	// make sure we don't exceed the bounds of our array
	if (start > MAX_QUEUE-1)
	{
		start = MAX_QUEUE-1;
	}

	// shuffle down operation

	for (int i = start; i > 0; i--)
	{
		data[i] = data[i - 1];
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	int tailIndex = 0;
	int dataArray[MAX_QUEUE];

	for (int i = 0; i < MAX_QUEUE; i++)
	{
		dataArray[i] = 0;
	}

	dataArray[tailIndex] = 1;

	printData(dataArray);


	for (int i = 0; i < 20; i++)
	{
		shuffleDown(dataArray, tailIndex);

		int randomNumber = distribution(generator);

		std::cout << "index:" << i << "," << randomNumber <<" tail: " <<tailIndex << std::endl << std::endl;

		insertHead(dataArray, tailIndex, randomNumber);

		printData(dataArray);

		std::cout << std::endl;
	}
	printData(dataArray);


	return 0;
}

