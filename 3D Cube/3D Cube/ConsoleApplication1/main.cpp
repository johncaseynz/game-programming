#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include <iostream>
#include <random>
#include <list>
#include <cmath>

float SCALE = 400;
float DISTANCE = 3.0f;

const int POINT_SZ = 8;
const int FACE_SZ = 4;

const int WIDTH = 800;
const int HEIGHT = 600;

void processEvents(sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
	}
}

float cartesianXToScreenX(float cartesianX, float width)
{
	return cartesianX+(width / 2.0f);
}

float cartesianYToScreenY(float cartesianY, float height)
{
	return (height / 2) - cartesianY;
}

float project(float world_point, float z)
{
	return world_point / (z + DISTANCE);
}

sf::Vector2f convert_point(sf::Vector3f point)
{
	float x = project(point.x, point.z) * SCALE;
	float y = project(point.y, point.z) * SCALE;

	float translateX = cartesianXToScreenX(x, WIDTH);
	float translateY = cartesianYToScreenY(y, HEIGHT);
	
	sf::Vector2f pnt;
	pnt.x = translateX;
	pnt.y = translateY;

	return pnt;
}

int main()
{
	sf::Vector3f points[POINT_SZ];

	points[0] = sf::Vector3f(0.9807444, 1.3712621, 0.39721626);
	points[1] = sf::Vector3f(1.0696751, 0.34589082, -1.3176322);
	points[2] = sf::Vector3f(1.0165715, -1.3712621, -0.293637);
	points[3] = sf::Vector3f(0.92764086, -0.34589082, 1.4212115);
	points[4] = sf::Vector3f(-1.0696751, -0.34589082, 1.3176322);
	points[5] = sf::Vector3f(-0.9807444, -1.3712621, -0.39721626);
	points[6] = sf::Vector3f(-0.92764086, 0.34589082, -1.4212115);
	points[7] = sf::Vector3f(-1.0165715, 1.3712621, 0.293637);

	//points[0] = sf::Vector3f(1, 1, 1);
	//points[1] = sf::Vector3f(1, 1, -1);
	//points[2] = sf::Vector3f(1, -1, -1);
	//points[3] = sf::Vector3f(1, -1, 1);
	//points[4] = sf::Vector3f(-1, -1, 1);
	//points[5] = sf::Vector3f(-1, -1, -1);
	//points[6] = sf::Vector3f(-1, 1, -1);
	//points[7] = sf::Vector3f(-1, 1, 1);

	int faces[FACE_SZ][FACE_SZ] = {
		{ 0, 1, 2, 3 }, // right
		{ 0, 1, 6, 7 }, // top
		{ 6, 7, 4, 5 }, // left
		{ 4, 5, 2, 3 }  // bottom
	};

	sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "SFML 3D Cube");
	window.setFramerateLimit(120);

	while (window.isOpen())
	{
		processEvents(window);

		window.clear();

		// draw points

		//for (int i = 0; i < POINT_SZ; i++)
		//{
		//	sf::CircleShape point;
		//	point.setRadius(3);
		//	point.setFillColor(sf::Color::Green);
		//	point.setOutlineColor(sf::Color::Green);

		//	sf::Vector3f world_point = points[i];
		//	sf::Vector2f screen_point = convert_point(world_point);

		//	std::cout << world_point.x << "," << world_point.y <<"," << world_point.z << std::endl;
		//	std::cout << screen_point.x <<"," << screen_point.y << std::endl;

		//	point.setPosition(screen_point);
		//	window.draw(point);
		//}



		for (int i = 0; i < FACE_SZ; i++)
		{
			sf::VertexArray line (sf::LinesStrip,5);
			
			for (int j = 0; j< FACE_SZ; j++)
			{
				sf::Vector3f world_point = points[faces[i][j]];
				sf::Vector2f screen_point = convert_point(world_point);

				std::cout << world_point.x << "," << world_point.y << "," << world_point.z << std::endl;
				std::cout << screen_point.x << "," << screen_point.y << std::endl;
				
				line[j].color = sf::Color::Green;
				line[j].position = screen_point;
			}

			line[4] = line[0]; // close the quad, does not happen automatically
			window.draw(line);
		}

		window.display();

		// translate all the points
		//for (int i = 0; i < POINT_SZ; i++)
		//{
		//	points[i].z = points[i].z - 0.5;
		//}
	}
}

