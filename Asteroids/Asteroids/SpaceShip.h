#pragma once

#include "Particle.h"
#include <random>
class SpaceShip : public Particle
{
	private:
		sf::CircleShape mainRocket;
		sf::RectangleShape leftThruster;
		sf::RectangleShape rightThruster;
		sf::Vector2f thrust;
		float angle;
	public:
		SpaceShip(const sf::FloatRect &boundary, const  sf::Vector2f &location, const float &mass);
		SpaceShip();
		~SpaceShip();

		void update(const sf::Time &deltaTime);

		sf::CircleShape& getCircleShape();
		void setCircleShape(sf::CircleShape& circle);

		void rotateLeft();
		void rotateRight();
		void fireRocket();
		void stop();
		void draw(sf::RenderWindow *window);
};

