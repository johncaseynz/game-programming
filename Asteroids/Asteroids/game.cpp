#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <random>

#include "Particle.h"
#include "SpaceShip.h"
#include "VectorMath.h"

bool right;
bool left;
bool rocket;

// process the state machine to turn on and off the different rocket thrusters 
void processEvents(Particle &particles, const sf::FloatRect &rect, sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
		{
			left = true;
			right = false;
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
		{
			left = false;
			right = true;
		}
		else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
		{
			left = false;
			right = false;
			rocket = true;
		}
		else if (event.type == sf::Event::KeyReleased)
		{
			left = false;
			right = false;
			rocket = false;
		}
	}
}

int main()
{
	sf::Time TimePerFrame = sf::seconds(1.f / 60); // update the screen in fixed time steps 60 times per second

	sf::ContextSettings context;
	context.antialiasingLevel = 10;

	sf::RenderWindow window(sf::VideoMode(1024, 768), "SFML Asteroids", sf::Style::Default, context);
	window.setFramerateLimit(60);


	const sf::FloatRect rect = sf::FloatRect(sf::Vector2f(0, 0), sf::Vector2f(window.getSize()));

	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;

	SpaceShip ship = SpaceShip(rect, sf::Vector2f(window.getSize().x / 2, window.getSize().y / 2), 1);


	while (window.isOpen())
	{
		if (left)
		{
			ship.rotateLeft();
		}
		else if (right)
		{
			ship.rotateRight();
		}
		else if (rocket)
		{
			ship.fireRocket();
		}
		else
		{
			ship.stop();
		}

		processEvents(ship, rect, window);
		timeSinceLastUpdate += clock.restart();

		while (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;
			processEvents(ship, rect, window);

			ship.update(TimePerFrame);
			ship.checkEdges();
		}

		window.clear();
		ship.draw(&window);
		window.display();
	}
}

