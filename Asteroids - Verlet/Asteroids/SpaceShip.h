#pragma once
#include "Particle.h"
class SpaceShip : public Particle
{
	private:
		sf::CircleShape circle;
		sf::RectangleShape left;
		sf::RectangleShape right;

	public:
		SpaceShip(const sf::FloatRect &boundary, const  sf::Vector2f &location, const float &mass);
		SpaceShip();
		~SpaceShip();

		virtual void update(const sf::Time &deltaTime);

		sf::CircleShape& getCircleShape();
		void setCircleShape(sf::CircleShape& circle);

		sf::RectangleShape& getLeftThruster();
		sf::RectangleShape& getRightThruster();
};

