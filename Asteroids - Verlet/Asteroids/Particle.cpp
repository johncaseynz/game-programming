#include "Particle.h"
#include "SpaceShip.h"

Particle::Particle()
{
	current = sf::Vector2f(0, 0);
	previous = sf::Vector2f(0.0000001, 0.0000001);
	acceleration = sf::Vector2f(0, 0);
	boundary = sf::FloatRect();

}

Particle::Particle(const sf::FloatRect &boundary, const  sf::Vector2f &location,const  float &mass)
{
	this->current = location;
	this->previous = location -sf::Vector2f(0.0000001, 0.0000001);
	this->acceleration = sf::Vector2f();
	this->boundary = boundary;
	this->mass = mass;

	this->topSpeed = 200;
}

void Particle::setAcceleration(const sf::Vector2f &acceleration)
{
	this->acceleration = acceleration;
}

sf::Vector2f Particle::getAcceleration()
{
	return acceleration;
}

sf::Vector2f Particle::getCurrentPosition()
{
	return current;
}

void Particle::applyForce(const sf::Vector2f &force)
{
	acceleration = acceleration + (force / mass);
}

void Particle::update(const sf::Time &deltaTime)
{
	sf::Vector2f temp = current + (current - previous) + acceleration * (deltaTime.asSeconds() * deltaTime.asSeconds());
	previous = current;
	current = temp;
}

int Particle::getLife()
{
	return life;
}

void Particle::checkEdges()
{
	if ((current.x > boundary.width))
	{
		current.x = 0;
		
	}

	if ((current.x < 0))
	{
		current.x = boundary.width;
	}

	if ((current.y > boundary.height))
	{
		current.y = 0;
	}

	if (current.y < 0)
	{
		current.y = boundary.height;
	}
}

Particle::~Particle()
{
}
