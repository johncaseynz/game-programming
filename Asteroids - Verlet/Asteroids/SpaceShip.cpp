#include "SpaceShip.h"

SpaceShip::SpaceShip(const sf::FloatRect & boundary, const sf::Vector2f & location, const float & mass) : Particle(boundary, location, mass)
{
	this->circle = sf::CircleShape(20); // radius of 20
	this->circle.setPointCount(3);
	this->circle.setOrigin(20, 20); // diameter is 40
	this->circle.setFillColor(sf::Color(127, 127, 127));
	this->circle.setOutlineColor(sf::Color::White);
	this->circle.setOutlineThickness(1);

	this->left = sf::RectangleShape(sf::Vector2f(5, 5));
	this->left.setOrigin(15, -10+2.5);
	this->left.setFillColor(sf::Color(127, 127, 127));
	this->left.setOutlineColor(sf::Color::White);
	this->left.setOutlineThickness(1);

	this->right = sf::RectangleShape(sf::Vector2f(5, 5));
	this->right.setOrigin(15, 10+2.5);
	this->right.setFillColor(sf::Color(127, 127, 127));
	this->right.setOutlineColor(sf::Color::White);
	this->right.setOutlineThickness(1);

	this->left.rotate(-30);
	this->right.setRotation(-30);
	this->circle.setRotation(-30);
}

SpaceShip::SpaceShip() : Particle()
{
	this->circle = sf::CircleShape(2);

	this->left = sf::RectangleShape();
	this->right = sf::RectangleShape();
}

SpaceShip::~SpaceShip()
{
}

sf::CircleShape& SpaceShip::getCircleShape()
{
	return this->circle;
}

void SpaceShip::setCircleShape(sf::CircleShape & circle)
{
	this->circle = circle;
}

sf::RectangleShape & SpaceShip::getLeftThruster()
{
	return this->left;
}

sf::RectangleShape & SpaceShip::getRightThruster()
{
	return this->right;
}

void SpaceShip::update(const sf::Time &deltaTime)
{
	Particle::update(deltaTime);
	circle.setPosition(current);

	left.setPosition(current);
	right.setPosition(current);
}
