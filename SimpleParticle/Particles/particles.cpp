#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include "Particle.h"
#include "VectorMath.h"

void processEvents(sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
	}

}

int main()
{
	sf::RenderWindow window(sf::VideoMode(800, 600), "Particle System - Gravity");
	window.setFramerateLimit(15);

	sf::CircleShape circle(20);
	circle.setFillColor(sf::Color(sf::Color::Red));
	circle.setPointCount(30);

	Particle particle(sf::FloatRect(sf::Vector2f(0, 0), sf::Vector2f(window.getSize())), sf::Vector2f(window.getSize().x / 2, 0), 1);
	
	particle.setAcceleration(sf::Vector2f(0, 4)); // .applyForce(sf::Vector2f(0, 9.8));

	while (window.isOpen())
	{
		processEvents(window);
		particle.update();
		particle.checkEdges();

		circle.setPosition(particle.getLocation());

		window.clear();
		window.draw(circle);
		window.display();
	}
}