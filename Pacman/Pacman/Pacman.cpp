#include "Pacman.h"
#include <cmath>
#include <SFML/Audio.hpp>

const int Pacman::left[FRAMES] = { 6,7,8 };
const int Pacman::right[FRAMES] = { 3,4,5 };
const int Pacman::down[FRAMES] = { 0,1,2 };
const int Pacman::up[FRAMES] = { 9,10,11 };

void Pacman::walk(Map &map)
{
	sf::Vector2f pos = sprite.getPosition();

	if (facing == RIGHT)
	{
		pos.x += 8;
		this->sprite.setTextureRect(sf::IntRect(right[frame] * 16, 0, 16, 16));
	}
	else if (facing == LEFT)
	{
		pos.x -= 8;
		this->sprite.setTextureRect(sf::IntRect(left[frame] * 16, 0, 16, 16));
	}
	else if (facing == DOWN)
	{
		pos.y += 8;
		this->sprite.setTextureRect(sf::IntRect(down[frame] * 16, 0, 16, 16));
	}
	else if (facing == UP)
	{
		pos.y -= 8;
		this->sprite.setTextureRect(sf::IntRect(up[frame] * 16, 0, 16, 16));
	}
	sprite.setPosition(pos);
	frame = (frame + 1) % 2;
}

void Pacman::move(Map &map, sf::Event & event)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		if (rowBoundary())
		{
			if (!map.isCollision(getRow(), getColumn() + 1) || !columnBoundary())
			{
				//if (sound.getStatus() == sf::SoundSource::Status::Stopped)
				//{
				//	sound.play();
				//}
				setFacing(Pacman::RIGHT);
				walk(map);
			}
		}
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		if (rowBoundary())
		{
			if (!map.isCollision(getRow(), getColumn() - 1) || !columnBoundary())
			{
				setFacing(Pacman::LEFT);
				walk(map);
			}
		}

	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		if (columnBoundary())
		{
			if (!map.isCollision(getRow() + 1, getColumn()) || !rowBoundary())
			{
				setFacing(Pacman::DOWN);
				walk(map);
			}
		}
	}
	else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		if (columnBoundary())
		{
			if (!map.isCollision(getRow() - 1, getColumn()) || !rowBoundary())
			{
				setFacing(Pacman::UP);
				walk(map);
			}
		}
	}
}

Pacman::Pacman()
{
	if (!texture.loadFromFile("assets/pacman.png"))
	{
		std::cout << "Error loading resource pacman.png" << std::endl;
	}

	sf::Image pacmanImage = texture.copyToImage();
	pacmanImage.createMaskFromColor(sf::Color(0, 0, 0), 0);

	if (!texture.loadFromImage(pacmanImage))
	{
		std::cout << "Error masking image resource pacman.png" << std::endl;
	}

	this->sprite.setTexture(texture);
	this->sprite.setScale(2, 2);
	this->sprite.setOrigin(4,4);
	
	this->facing = RIGHT;

	this->sprite.setTextureRect(sf::IntRect(right[frame]*16, 0,16,16));
}




Pacman::~Pacman()
{
}
