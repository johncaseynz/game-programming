#include "GameSprite.h"
#include "Ghost.h"
#include "Pacman.h"

#pragma once
class GhostBFS : public Ghost
{
	public:
		GhostBFS(Pacman *pacman,const char* filename);		
		void walk(Map &map, sf::RenderWindow &window, bool debug);
		~GhostBFS();
private:
		int bfs(Map &map, std::unordered_map <int, std::list<int>> graph, sf::RenderWindow &window, int source, int target, bool debug);
};

