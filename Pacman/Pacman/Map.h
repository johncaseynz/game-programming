#pragma once

#include <SFML/Graphics.hpp>
#include <iostream>
#include <unordered_map>
#include <unordered_set>
#include <queue>

class Map
{
private:
	std::unordered_map <int, std::list<int>> graph;
	
public:
	std::unordered_map <int, std::list<int>>* getGraph();
	void createGraph();
	int Map::convert2Dto1D(int row, int col);
	std::pair <int, int> convert1Dto2D(int id);

	static const int ROW_COUNT = 31;
	static const int COLUMN_COUNT = 28;

	enum Tile {
		TileOut = -1,
		TileEmpty = 0,
		TileWall = 1,
		TileGate = 2,
		TileDot = 3,
		TileEnergizer = 4,
		TilePacman = 5,
		TileRed = 6,
		TileCyan = 7,
		TilePink = 8,
		TileOrange = 9,
		TileIntersection = 10
	};
	void drawGrid(sf::RenderWindow &window);
	void drawGraph(std::unordered_map <int, std::list<int>> graph, sf::RenderWindow &window);
	Tile getTile(int row, int column);
	bool isTileIntersection(int row, int column);
	bool isCollision(int row, int column);
	sf::Sprite getSprite();

	void printAdjacencyList();

	Map();
	~Map();

	private:
		sf::Texture texture;
		sf::Sprite sprite;
		static const int Map::DEFAULT_MAP[Map::ROW_COUNT][COLUMN_COUNT];
		static const int Map::INTERSECTION_MAP[ROW_COUNT][COLUMN_COUNT];
};

