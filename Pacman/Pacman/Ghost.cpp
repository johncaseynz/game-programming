#include "Ghost.h"
#include "Pacman.h"

Ghost::Ghost(Pacman *pacman,const char* filename)
{
	this->pacman = pacman;

	if (!texture.loadFromFile(filename))
	{
		std::cout << "Error loading resource " <<filename << std::endl;
	}

	sf::Image ghostImage = texture.copyToImage();
	ghostImage.createMaskFromColor(sf::Color(0, 0, 0), 0);

	if (!texture.loadFromImage(ghostImage))
	{
		std::cout << "Error masking image resource " <<filename << std::endl;
	}

	this->sprite.setTexture(texture);
	this->sprite.setScale(2, 2);
	this->sprite.setOrigin(4, 4);

	this->facing = RIGHT;

	this->sprite.setTextureRect(sf::IntRect(right[frame] * 16, 0, 16, 16));
}
void Ghost::draw(sf::RenderWindow & window)
{
	window.draw(getSprite());
}
// implementation of the basic chase AI algorithm
void Ghost::walk(Map & map, sf::RenderWindow & window, bool debug)
{
	sf::Vector2f ghostLoc = sprite.getPosition();
	sf::Vector2f pacmanLoc = pacman->getSprite().getPosition();

	sf::Vector2f pos = sprite.getPosition();
	if (ghostLoc.x < pacmanLoc.x)
	{
		if (rowBoundary())
		{
			if (!columnBoundary())
			{
				moveRIGHT(pos);
			}
			else
				if (!map.isCollision(getRow(), getColumn() + 1))
				{
					moveRIGHT(pos);
				}
		}
		else
		{
			if (!columnBoundary())
			{
				moveRIGHT(pos);
			}
		}
	}
	else if (ghostLoc.x > pacmanLoc.x)
	{
		if (rowBoundary())
		{
			if (!columnBoundary())
			{
				moveLEFT(pos);
			}
			else
				if (!map.isCollision(getRow(), getColumn() - 1))
				{
					moveLEFT(pos);
				}
		}
		else
		{
			if (!columnBoundary())
			{
				moveLEFT(pos);
			}
		}
	}

	if (pos.x == ghostLoc.x)
	{
		if (ghostLoc.y < pacmanLoc.y)
		{
			if (columnBoundary())
			{
				if (!rowBoundary())
				{
					moveDOWN(pos);
				}
				else
					if (!map.isCollision(getRow() + 1, getColumn()))
					{
						moveDOWN(pos);
					}
			}
			else
			{
				if (!rowBoundary())
				{
					moveDOWN(pos);
				}
			}
		}
		else if (ghostLoc.y > pacmanLoc.y)
		{
			if (columnBoundary())
			{
				if (!rowBoundary())
				{
					moveUP(pos);
				}
				else
					if (!map.isCollision(getRow() - 1, getColumn()))
					{
						moveUP(pos);
					}
			}
			else
			{
				if (!rowBoundary())
				{
					moveUP(pos);
				}
			}
		}
	}

	sprite.setPosition(pos);
	frame = (frame + 1) % 3;
}

void Ghost::moveRIGHT(sf::Vector2f &pos)
{
	facing = RIGHT;

	pos.x = pos.x + 1;
	this->sprite.setTextureRect(sf::IntRect(right[frame] * 16, 0, 16, 16));
}

void Ghost::moveLEFT(sf::Vector2f &pos)
{
	facing = LEFT;

	pos.x = pos.x - 1;
	this->sprite.setTextureRect(sf::IntRect(left[frame] * 16, 0, 16, 16));
}

void Ghost::moveDOWN(sf::Vector2f &pos)
{
	facing = DOWN;

	pos.y = pos.y + 1;
	this->sprite.setTextureRect(sf::IntRect(down[frame] * 16, 0, 16, 16));
}

void Ghost::moveUP(sf::Vector2f &pos)
{
	facing = UP;

	pos.y = pos.y - 1;

	this->sprite.setTextureRect(sf::IntRect(up[frame] * 16, 0, 16, 16));
}