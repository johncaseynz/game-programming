#include "Pacman.h"
#pragma once
#include "GameSprite.h"
class Ghost :
	public GameSprite
{
public:
	Ghost(Pacman *pacman,const char* filename);
	void draw(sf::RenderWindow &window);
	void walk(Map &map, sf::RenderWindow &window, bool debug);

	void moveRIGHT(sf::Vector2f &pos);
	void moveLEFT(sf::Vector2f &pos);
	void moveDOWN(sf::Vector2f &pos);
	void moveUP(sf::Vector2f &pos);
protected:
	Pacman *pacman;
};

