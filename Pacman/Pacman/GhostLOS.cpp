#include "GhostLOS.h"
#include "VectorMath.h"


GhostLOS::GhostLOS(Pacman * pacman, const char* filename) : Ghost(pacman, filename)
{

}

GhostLOS::~GhostLOS()
{
}

float GhostLOS::distance(int row, int col)
{
	sf::Vector2f ghostLoc = sf::Vector2f(col, row);
	sf::Vector2f pacmanLoc = sf::Vector2f(pacman->getColumn(), pacman->getRow());

	sf::Vector2f distance = pacmanLoc - ghostLoc;

	float dist = vm::magnitude(distance);

	return dist;
}

float GhostLOS::distDraw(int row, int column, sf::RenderWindow &window)
{
	float dist = distance(row, column);

	sf::Vector2f origin((getColumn()) * 16 + 8, (getRow() + 1) * 16 + 8);
	sf::Vector2f target(pacman->getColumn() * 16 + 8, pacman->getRow() * 16 + 8);

	sf::Vertex line[2] = {
		sf::Vertex(origin,sf::Color::Red),
		sf::Vertex(target,sf::Color::Red)
	};
	window.draw(line, 2, sf::Lines);

	return dist;
}

void GhostLOS::move(bool left, sf::Vector2f & pos, bool right, bool up, bool down)
{
	if (left)
	{
		moveLEFT(pos);
	}
	else if (right)
	{
		moveRIGHT(pos);
	}
	else if (up)
	{
		moveUP(pos);
	}
	else if (down)
	{
		moveDOWN(pos);
	}
}

void GhostLOS::walk(Map &map, sf::RenderWindow &window, bool debug)
{
	sf::Vector2f pos = this->sprite.getPosition();

	bool up = false;
	bool down = false;
	bool left = false;
	bool right = false;

	float min = 0;
	bool init = false;

	if (map.isTileIntersection(getRow(), getColumn()) && rowBoundary() && columnBoundary())
	{
		if (!map.isCollision(getRow() + 1, getColumn()) || !rowBoundary()) // down
		{
			float tmp = distDraw(getRow() + 1, getColumn(), window);
			min = tmp;

			up = false;
			down = true;
			left = false;
			right = false;

			init = true;
		}
		if (!map.isCollision(getRow() - 1, getColumn()) || !rowBoundary()) // up
		{
			float tmp = distDraw(getRow() - 1, getColumn(), window);
			if ((tmp < min) || !init)
			{
				min = tmp;

				up = true;
				down = false;
				left = false;
				right = false;

				init = true;
			}
		}
		if (!map.isCollision(getRow(), getColumn() + 1) || !columnBoundary()) //right
		{
			float tmp = distDraw(getRow(), getColumn() + 1, window);
			if ((tmp < min) || !init)
			{
				min = tmp;

				up = false;
				down = false;
				left = false;
				right = true;

				init = true;
			}
		}
		if (!map.isCollision(getRow(), getColumn() - 1) || !columnBoundary()) //left
		{
			float tmp = distDraw(getRow(), getColumn() - 1, window);

			if ((tmp < min) || !init)
			{
				min = tmp;

				up = false;
				down = false;
				left = true;
				right = false;

				init = true;
			}
		}
		move(left, pos, right, up, down);

	}
	else if (facing == UP)
	{
		moveUP(pos);
	}
	else if (facing == DOWN)
	{
		moveDOWN(pos);
	}
	else if (facing == LEFT)
	{
		moveLEFT(pos);
	}
	else if (facing == RIGHT)
	{
		moveRIGHT(pos);
	}


	sprite.setPosition(pos);
	frame = (frame + 1) % 3;

}