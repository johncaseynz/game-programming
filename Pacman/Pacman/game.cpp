#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <iostream>

#include <unordered_map>
#include <unordered_set>
#include <utility>
#include <list>


#include "Pacman.h"
#include "Ghost.h"
#include "GhostDFS.h"
#include "GhostBFS.h"
#include "GhostLOS.h"
#include "Map.h"

const int BLOCK_SZ = 16;

void drawEmptyTiles(Map &map, sf::RenderWindow &window)
{
	for (int row = 0; row < map.COLUMN_COUNT; row++)
	{
		for (int col = 0; col < map.ROW_COUNT; col++)
		{
			Map::Tile tile = map.getTile(col, row);
			if (tile == Map::Tile::TilePink || tile == Map::Tile::TileCyan || tile == Map::Tile::TileRed || tile == Map::Tile::TileOrange || tile == Map::Tile::TilePacman || tile == Map::Tile::TileEmpty || tile == Map::Tile::TileDot || tile == Map::Tile::TileEnergizer)
			{
				sf::RectangleShape rectangle;
				rectangle.setPosition(row * BLOCK_SZ, col * BLOCK_SZ);
				rectangle.setSize(sf::Vector2f(BLOCK_SZ, BLOCK_SZ));

				rectangle.setFillColor(sf::Color::Cyan);

				window.draw(rectangle);
			}

			if (map.isTileIntersection(col, row))
			{
				sf::RectangleShape rectangle;
				rectangle.setPosition(row * BLOCK_SZ, col * BLOCK_SZ);
				rectangle.setSize(sf::Vector2f(BLOCK_SZ, BLOCK_SZ));

				rectangle.setFillColor(sf::Color::Red);

				window.draw(rectangle);
			}

		}
	}
}

int main()
{
	bool drawPacman = true;
	bool drawGhost = true;
	bool debug = true;
	bool drawGridCells = false;
	bool drawEmptyPath = false;

	sf::SoundBuffer buffer;
	if (!buffer.loadFromFile("assets/pacman_chomp.wav"))
	{
		std::cout << "Could not load resource: assets/pacman_chomp.wav" << std::endl;
	}
	

	sf::Sound sound;
	sound.setBuffer(buffer);

	sf::Music music;
	if (!music.openFromFile("assets/pacman_start.wav"))
	{
		std::cout << "Could not load resource: assets/pacman_start.wav" << std::endl;
	}
	music.play();

	// create the window
	sf::RenderWindow window(sf::VideoMode(448, 496), "Pacman");
	window.setFramerateLimit(60);

	Pacman pacman;
	Map map;
	map.printAdjacencyList();


	pacman.setPosition(1, 1);

	GhostDFS dfs(&pacman, "assets/ghostRed.png");
	dfs.setPosition(6, 1);

	GhostBFS red (&pacman,"assets/ghostRed.png");
	red.setPosition(3, 1);

	Ghost blue(&pacman,"assets/ghostCyan.png");
	blue.setPosition(3, 5);

	GhostLOS los(&pacman, "assets/ghostCyan.png");
	los.setPosition(3, 5);

	// run the program as long as the window is open
	while (window.isOpen())
	{
		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		while (window.pollEvent(event))
		{
			// "close requested" event: we close the window
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}
			else if (event.type == sf::Event::KeyPressed)
			{
				pacman.move(map, event);
			}
		}
		window.clear();

		if (debug)
		{
			std::cout << "actual position: (" << pacman.getSprite().getPosition().x << ", " << pacman.getSprite().getPosition().y << ")" << std::endl;
			std::cout << "calculated position: (" << pacman.getSprite().getPosition().x/16.0f << ", " << pacman.getSprite().getPosition().y/16.0f << ")" << std::endl;
			std::cout << "grid position: (" << pacman.getColumn() << "," << pacman.getRow() << ")" << std::endl;

			std::cout << "map tile: " << map.getTile(pacman.getRow(), pacman.getColumn()) << std::endl;
		}

		window.draw(map.getSprite());

		dfs.walk(map, window, debug);
		red.walk(map, window, debug);
		blue.walk(map, window, debug);
		los.walk(map, window, debug);
		
		if (drawGridCells)
		{
			if (drawEmptyPath)
			{
				drawEmptyTiles(map, window);
			}

			map.drawGrid(window);
		}

		if (drawGhost)
		{
			dfs.draw(window);
			red.draw(window);
			blue.draw(window);
			los.draw(window);
		}
		if (drawPacman)
		{
			window.draw(pacman.getSprite());
		}
		if (debug)
		{
			sf::RectangleShape boundingBox;
			boundingBox.setSize(sf::Vector2f(32, 32));
			boundingBox.setOrigin(8, 8);
			boundingBox.setPosition(pacman.getSprite().getPosition());
			boundingBox.setFillColor(sf::Color(0, 0, 0, 0));
			boundingBox.setOutlineColor(sf::Color::Red);
			boundingBox.setOutlineThickness(3);

			window.draw(boundingBox);
		}

		// drawGraph(graph, window);

		window.display();
	}

}



