#pragma once
#include "Map.h"
const int FRAMES = 3;

class GameSprite
{
public:
	enum Facing
	{
		LEFT,
		RIGHT,
		UP,
		DOWN
	};

	bool rowBoundary();
	bool columnBoundary();

	int getColumn();
	int getRow();

	void walk(Map &map);

	GameSprite();
	~GameSprite();

	sf::Sprite getSprite();
	void setFacing(Facing facing);
	void setPosition(int row, int column);

	protected:
		Facing facing;

		int frame = 0;

		static const int left[FRAMES];
		static const int right[FRAMES];
		static const int down[FRAMES];
		static const int up[FRAMES];
		sf::Sprite sprite;
		sf::Texture texture;
};

