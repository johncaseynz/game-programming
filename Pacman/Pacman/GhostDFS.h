#include <unordered_set>

#pragma once
#include "Ghost.h"
class GhostDFS :
	public Ghost
{
public:
	GhostDFS(Pacman *pacman, const char* filename);
	void walk(Map &map, sf::RenderWindow &window, bool debug);
	int dfs(Map &map, sf::RenderWindow & window, std::unordered_map<int, std::list<int>> *graph, std::list<int> edges, int target, std::list<int> &path, bool debug);
	~GhostDFS();
private:
	std::unordered_set <int> visited;
};

