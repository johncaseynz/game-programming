#pragma once
#include "Ghost.h"
class GhostLOS :
	public Ghost
{
public:
	GhostLOS(Pacman * pacman, const char * filename);
	~GhostLOS();
	
	void walk(Map & map, sf::RenderWindow & window, bool debug);
private:
	void move(bool left, sf::Vector2f & pos, bool right, bool up, bool down);
	float distance(int row, int col);
	float distDraw(int row, int column, sf::RenderWindow & window);
};

