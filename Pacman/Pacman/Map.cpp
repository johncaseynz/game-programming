#include "Map.h"

const int BLOCK_SZ = 16;

const int Map::INTERSECTION_MAP[ROW_COUNT][COLUMN_COUNT] = {
	{ 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01 },
	{ 01, 10, 03, 03, 03, 03, 10, 03, 03, 03, 03, 03, 10, 01, 01, 10, 03, 03, 03, 03, 03, 10, 03, 03, 03, 03, 10, 01 },
	{ 01, 03, 01, 01, 01, 01, 03, 01, 01, 01, 01, 01, 03, 01, 01, 03, 01, 01, 01, 01, 01, 03, 01, 01, 01, 01, 03, 01 },
	{ 01, 04, 01, 01, 01, 01, 03, 01, 01, 01, 01, 01, 03, 01, 01, 03, 01, 01, 01, 01, 01, 03, 01, 01, 01, 01, 04, 01 },
	{ 01, 03, 01, 01, 01, 01, 03, 01, 01, 01, 01, 01, 03, 01, 01, 03, 01, 01, 01, 01, 01, 03, 01, 01, 01, 01, 03, 01 },
	{ 01, 10, 03, 03, 03, 03, 10, 03, 03, 10, 03, 03, 10, 03, 03, 10, 03, 03, 10, 03, 03, 10, 03, 03, 03, 03, 10, 01 },
	{ 01, 03, 01, 01, 01, 01, 03, 01, 01, 03, 01, 01, 01, 01, 01, 01, 01, 01, 03, 01, 01, 03, 01, 01, 01, 01, 03, 01 },
	{ 01, 03, 01, 01, 01, 01, 03, 01, 01, 03, 01, 01, 01, 01, 01, 01, 01, 01, 03, 01, 01, 03, 01, 01, 01, 01, 03, 01 },
	{ 01, 10, 03, 03, 03, 03, 10, 01, 01, 10, 03, 03, 10, 01, 01, 10, 03, 03, 10, 01, 01, 10, 03, 03, 03, 03, 10, 01 },
	{ 01, 01, 01, 01, 01, 01, 03, 01, 01, 01, 01, 01, 00, 01, 01, 00, 01, 01, 01, 01, 01, 03, 01, 01, 01, 01, 01, 01 },
	{ 01, 01, 01, 01, 01, 01, 03, 01, 01, 01, 01, 01, 00, 01, 01, 00, 01, 01, 01, 01, 01, 03, 01, 01, 01, 01, 01, 01 },
	{ 01, 01, 01, 01, 01, 01, 03, 01, 01, 10, 00, 00, 10, 06, 06, 10, 00, 00, 10, 01, 01, 03, 01, 01, 01, 01, 01, 01 },
	{ 01, 01, 01, 01, 01, 01, 03, 01, 01, 00, 01, 01, 01, 02, 02, 01, 01, 01, 00, 01, 01, 03, 01, 01, 01, 01, 01, 01 },
	{ 01, 01, 01, 01, 01, 01, 03, 01, 01, 00, 01, 01, 01, 00, 00, 01, 01, 01, 00, 01, 01, 03, 01, 01, 01, 01, 01, 01 },
	{ 00, 00, 00, 00, 00, 00, 10, 00, 00, 10, 01, 07, 07,  8,  8,  9,  9, 01, 10, 00, 00, 10, 00, 00, 00, 00, 00, 00 },
	{ 01, 01, 01, 01, 01, 01, 03, 01, 01, 00, 01, 01, 01, 01, 01, 01, 01, 01, 00, 01, 01, 03, 01, 01, 01, 01, 01, 01 },
	{ 01, 01, 01, 01, 01, 01, 03, 01, 01, 00, 01, 01, 01, 01, 01, 01, 01, 01, 00, 01, 01, 03, 01, 01, 01, 01, 01, 01 },
	{ 01, 01, 01, 01, 01, 01, 03, 01, 01, 10, 00, 00, 00, 00, 00, 00, 00, 00, 10, 01, 01, 03, 01, 01, 01, 01, 01, 01 },
	{ 01, 01, 01, 01, 01, 01, 03, 01, 01, 00, 01, 01, 01, 01, 01, 01, 01, 01, 00, 01, 01, 03, 01, 01, 01, 01, 01, 01 },
	{ 01, 01, 01, 01, 01, 01, 03, 01, 01, 00, 01, 01, 01, 01, 01, 01, 01, 01, 00, 01, 01, 03, 01, 01, 01, 01, 01, 01 },
	{ 01, 10, 03, 03, 03, 03, 10, 03, 03, 10, 03, 03, 10, 01, 01, 10, 03, 03, 10, 03, 03, 10, 03, 03, 03, 03, 10, 01 },
	{ 01, 03, 01, 01, 01, 01, 03, 01, 01, 01, 01, 01, 03, 01, 01, 03, 01, 01, 01, 01, 01, 03, 01, 01, 01, 01, 03, 01 },
	{ 01, 03, 01, 01, 01, 01, 03, 01, 01, 01, 01, 01, 03, 01, 01, 03, 01, 01, 01, 01, 01, 03, 01, 01, 01, 01, 03, 01 },
	{ 01, 10, 03, 10, 01, 01, 10, 03, 03, 10, 03, 03, 10, 05, 05, 10, 03, 03, 10, 03, 03, 10, 01, 01, 10, 03, 10, 01 },
	{ 01, 01, 01, 03, 01, 01, 03, 01, 01, 03, 01, 01, 01, 01, 01, 01, 01, 01, 03, 01, 01, 03, 01, 01, 03, 01, 01, 01 },
	{ 01, 01, 01, 03, 01, 01, 03, 01, 01, 03, 01, 01, 01, 01, 01, 01, 01, 01, 03, 01, 01, 03, 01, 01, 03, 01, 01, 01 },
	{ 01, 10, 03, 10, 03, 03, 10, 01, 01, 10, 03, 03, 10, 01, 01, 10, 03, 03, 10, 01, 01, 10, 03, 03, 10, 03, 10, 01 },
	{ 01, 03, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 03, 01, 01, 03, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 03, 01 },
	{ 01, 03, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 03, 01, 01, 03, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 03, 01 },
	{ 01, 10, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 10, 03, 03, 10, 03, 03, 03, 03, 03, 03, 03, 03, 03, 03, 10, 01 },
	{ 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01, 01 }
};


const int Map::DEFAULT_MAP[ROW_COUNT][COLUMN_COUNT] = {
	{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 },
	{ 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1 },
	{ 1, 3, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 3, 1 },
	{ 1, 4, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 4, 1 },
	{ 1, 3, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 3, 1 },
	{ 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1 },
	{ 1, 3, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 3, 1 },
	{ 1, 3, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 3, 1 },
	{ 1, 3, 3, 3, 3, 3, 3, 1, 1, 3, 3, 3, 3, 1, 1, 3, 3, 3, 3, 1, 1, 3, 3, 3, 3, 3, 3, 1 },
	{ 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 3, 1, 1, 0, 0, 0, 0, 6, 6, 0, 0, 0, 0, 1, 1, 3, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 3, 1, 1, 0, 1, 1, 1, 2, 2, 1, 1, 1, 0, 1, 1, 3, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 3, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 3, 1, 1, 1, 1, 1, 1 },
	{ 0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 1, 7, 7, 8, 8, 9, 9, 1, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0 },
	{ 1, 1, 1, 1, 1, 1, 3, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 3, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 3, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 3, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 3, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 3, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 3, 1, 1, 1, 1, 1, 1 },
	{ 1, 1, 1, 1, 1, 1, 3, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 3, 1, 1, 1, 1, 1, 1 },
	{ 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1 },
	{ 1, 3, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 3, 1 },
	{ 1, 3, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 3, 1 },
	{ 1, 4, 3, 3, 1, 1, 3, 3, 3, 3, 3, 3, 3, 5, 5, 3, 3, 3, 3, 3, 3, 3, 1, 1, 3, 3, 4, 1 },
	{ 1, 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 1 },
	{ 1, 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 1 },
	{ 1, 3, 3, 3, 3, 3, 3, 1, 1, 3, 3, 3, 3, 1, 1, 3, 3, 3, 3, 1, 1, 3, 3, 3, 3, 3, 3, 1 },
	{ 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1 },
	{ 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1 },
	{ 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1 },
	{ 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1 }
};


Map::Tile Map::getTile(int row, int column)
{
	return Tile(DEFAULT_MAP[row][column]);
}

bool Map::isTileIntersection(int row, int column)
{
	Map::Tile tile = Tile(INTERSECTION_MAP[row][column]);
	if (tile == Map::Tile::TileIntersection)
	{
		return true;
	}
	return false;
}


bool Map::isCollision(int row, int column)
{
	Map::Tile tile = getTile(row, column);
	if (tile != Map::Tile::TileWall && tile != Map::Tile::TileGate)
	{
		return false;
	}

	return true;
}

sf::Sprite Map::getSprite()
{
	return sprite;
}

std::pair <int, int> Map::convert1Dto2D(int id)
{
	int row = id / Map::COLUMN_COUNT;
	int col = id % Map::COLUMN_COUNT;

	return std::pair<int, int>(row, col);
}

int Map::convert2Dto1D(int row, int col)
{
	return (row * Map::COLUMN_COUNT) + col;
}

void Map::printAdjacencyList()
{
	std::cout << "{" << std::endl;
	std::cout << "nodes\":[" << std::endl;

	for (int row = 0; row < ROW_COUNT; row++)
	{
		for (int col = 0; col < COLUMN_COUNT; col++)
		{
			if (isTileIntersection(row, col))
			{
				std::cout << "{\"name\":" <<"\"" <<row << "_" << col << "\", \"group\" : 1}," << std::endl;
			}
		}
	}

	std::cout << "]," << std::endl;
}

Map::Map()
{
	if (!texture.loadFromFile("assets/map.png"))
	{
		std::cout << "Error loading resource map.bmp" << std::endl;
	}

	sprite.setTexture(texture);
	sprite.setScale(2, 2);

	createGraph();
}


Map::~Map()
{
}

void Map::drawGraph(std::unordered_map <int, std::list<int>> graph, sf::RenderWindow &window)
{
	int id = convert2Dto1D(1, 1);
	int target = convert2Dto1D(30, 27);;

	std::unordered_set<int> visited;

	std::queue<int> queue;

	int root = id;

	queue.push(root); // enqueue the root node
	visited.insert(root);

	std::list<int> path;

	const int parentSz = Map::COLUMN_COUNT * Map::ROW_COUNT;

	int parents[parentSz];

	parents[root] = -1;

	while (!queue.empty())
	{
		int node = queue.front();
		queue.pop();
		path.push_back(node);

		std::list <int> edges = graph[node];

		std::pair<int, int> pair = convert1Dto2D(node);

		sf::CircleShape circle;
		circle.setOrigin(-4, -4);
		circle.setPosition(pair.second * BLOCK_SZ, pair.first * BLOCK_SZ);
		circle.setRadius(4.0f);

		sf::Color color = sf::Color::Red;

		circle.setFillColor(color);

		window.draw(circle);


		for (std::list<int>::iterator it = edges.begin(); it != edges.end(); it++)
		{
			int nde = *it;

			std::pair<int, int> adj = convert1Dto2D(nde);

			sf::Vertex line[2] = {
				sf::Vertex(sf::Vector2f((pair.second * BLOCK_SZ) + 8,(pair.first * BLOCK_SZ) + 8),sf::Color::Red),
				sf::Vertex(sf::Vector2f((adj.second * BLOCK_SZ) + 8, (adj.first * BLOCK_SZ) + 8),sf::Color::Red)
			};
			window.draw(line, 2, sf::Lines);

			if (visited.count(nde) == 0)
			{
				visited.insert(nde);
				queue.push(nde);

				parents[nde] = node;
			}


		}
	}



}



std::unordered_map<int, std::list<int>>* Map::getGraph()
{
	return &graph;
}

void Map::createGraph()
{
	for (int row = 0; row < Map::ROW_COUNT; row++)
	{
		for (int col = 0; col < Map::COLUMN_COUNT; col++)
		{
			int id = convert2Dto1D(row, col);

			if (!isCollision(row, col))
			{
				std::list <int> edges;

				if (!isCollision(row - 1, col))
				{
					edges.emplace_front(convert2Dto1D(row - 1, col));
				}
				if (!isCollision(row + 1, col))
				{
					edges.emplace_front(convert2Dto1D(row + 1, col));
				}
				if (!isCollision(row, col - 1))
				{
					edges.emplace_front(convert2Dto1D(row, col - 1));
				}
				if (!isCollision(row, col + 1))
				{
					edges.emplace_front(convert2Dto1D(row, col + 1));
				}

				graph[id] = edges;
			}
		}
	}
}
void Map::drawGrid(sf::RenderWindow &window)
{
	for (unsigned int y = BLOCK_SZ; y < window.getSize().y;y += BLOCK_SZ)
	{
		sf::Vertex line[2] = {
			sf::Vertex(sf::Vector2f(0.0f,y),sf::Color(159,159,159,255)),
			sf::Vertex(sf::Vector2f(window.getSize().x,y),sf::Color(159,159,159,255))
		};
		window.draw(line, 2, sf::Lines);
	}

	for (unsigned int x = BLOCK_SZ; x < window.getSize().x;x += BLOCK_SZ)
	{
		sf::Vertex line[2] = {
			sf::Vertex(sf::Vector2f(x,0),sf::Color(159,159,159,255)),
			sf::Vertex(sf::Vector2f(x,window.getSize().y),sf::Color(159,159,159,255))
		};
		window.draw(line, 2, sf::Lines);
	}
}