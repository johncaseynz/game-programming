#include "GL/glew.h"
#include "SFML/Graphics.hpp"
#include "SFML/OpenGL.hpp" // SFML OpenGL include
#include <iostream>
#include <cmath>
#include "main.h"
#include "VectorMath.h"

class Particle
{
private:
	GLUquadric *quad = gluNewQuadric();
public:
	sf::Vector3f location;
	sf::Vector3f velocity;
	sf::Vector3f acceleration;

	void integrate()
	{
		location = location + velocity;
		velocity = velocity + acceleration;
	}

	float RADIUS()
	{
		return 1.0f; // sphere radius in world co-ordinates
	}

	bool collision(sf::Vector3f normal, sf::Vector3f plane)
	{
		// project the distance between the particle location and the plane along the normal vector
		sf::Vector3f projection = vm::project(location - plane, normal);
		float distance = vm::magnitude(projection);

		std::cout << "distance:" << distance << std::endl;

		if (distance <= RADIUS())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	void render()
	{
		glTranslatef(location.x, location.y, location.z);
		glColor3f(1, 0, 0);
		gluSphere(quad, RADIUS(), 8, 8);
	}
};

class Plane
{
	private:
		GLfloat vertices[12] = {10.f, -10.f, -10.f ,
								-10.0f, -10.f, -10.f,
								-10.f, -10.f, 10.f,
								10.f, -10.f, 10.f
		};
		sf::Vector3f planeNormal;

		sf::Vector3f bottomRightCorner;
		sf::Vector3f bottomLeftCorner;
		sf::Vector3f topLeftCorner;
	public:
		Plane()
		{
			// set the corners of the triangle
			bottomRightCorner = sf::Vector3f(10.0f, -10.0f, -10.0f);
			bottomLeftCorner = sf::Vector3f(-10.0f, -10.0f, -10.0f);
			topLeftCorner = sf::Vector3f(-10.0f, -10.0f, 10.0f);

			// calculate the normal of the plane
			planeNormal = vm::cross(topLeftCorner - bottomLeftCorner,bottomRightCorner - bottomLeftCorner);
			planeNormal = vm::normalize(planeNormal);
		}

		void collision(Particle &particle)
		{
			if (particle.collision(planeNormal, bottomRightCorner))
			{
				// project the velocity along the normal vector of the corner

				sf::Vector3f reflected = 2.0f * vm::project(particle.velocity, planeNormal);
				particle.velocity = particle.velocity - reflected;
			}
		}

		void render()
		{
			glTranslatef(0, 0, -15);

			glEnableClientState(GL_VERTEX_ARRAY);

			glVertexPointer(3, GL_FLOAT, 0, vertices);
			glColor3f(0, 1, 0);
			glDrawArrays(GL_QUADS, 0, 4);

			glDisableClientState(GL_VERTEX_ARRAY);
		}
};

int main()
{
	// Create the main window
	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML OpenGL");

	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // nicest perspective correction calculations

	//prepare OpenGL surface for HSR
	glClearDepth(1.f); // clear the z-buffer/depth buffer completely +1.0f is the furtherest away
	glClearColor(0.3f, 0.3f, 0.3f, 0.f); // set the background colour for when we clear the screen RGBA values in the 0.0 to 1.0 range. This gives us a nice grey background.

	setupProjectionMatrix();
	setupLighting();

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);

	// setup game objects
	Plane plane;
	Particle particle;

	particle.location.x = -10;
	particle.location.y = 10;
	particle.location.z = 0;

	particle.acceleration.y = -0.0001f;
	particle.acceleration.x = 0.000001f;

	while (window.isOpen())
	{
		processEvents(window);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear z-buffer and set previously selected colour

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

		particle.integrate();
		plane.collision(particle);

		plane.render();
		particle.render();

		window.display();
	}

	return EXIT_SUCCESS;
}

void processEvents(sf::RenderWindow &window)
{
	// Process events
	sf::Event Event;
	while (window.pollEvent(Event))
	{
		// Close window : exit
		if (Event.type == sf::Event::Closed)
			window.close();
	}
}

void setupLighting()
{
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	glShadeModel(GL_SMOOTH);

	GLfloat DIFFUSE_COLOR[] = { 0.8f, 0.8f, 0.8f, 1.0f };
	GLfloat SPECULAR_COLOR[] = { 1.25f, 1.25f, 1.25f, 1.0f };
	GLfloat LIGHT_POSITION[] = { 0.0f, 3, 0, -8.0f };
	glLightfv(GL_LIGHT0, GL_POSITION, LIGHT_POSITION);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, DIFFUSE_COLOR);
	glLightfv(GL_LIGHT0, GL_SPECULAR, SPECULAR_COLOR);

	glEnable(GL_COLOR_MATERIAL); // hack so we don't have to define color materials
}

void setupProjectionMatrix()
{
	// GL_PROJECTION what we actually see
	glMatrixMode(GL_PROJECTION); // Select the builtin projection matrix
	glLoadIdentity();  // reset the projection matrix by loading the projection identity matrix

	GLdouble fovY = 90;
	GLdouble aspect = 1.0f;
	GLdouble zNear = 1.0f;
	GLdouble zFar = 300.0f;

	const GLdouble pi = 3.1415926535897932384626433832795;
	GLdouble fW, fH;

	fH = tan(fovY / 360 * pi) * zNear;
	fW = fH * aspect;

	float angle = 0;

	// define a perspective projection
	glFrustum(-fW, fW, -fH, fH, zNear, zFar); // multiply the set matrix; by a perspective matrix
}
