#pragma once

void setupProjectionMatrix();

void setupLighting();

void processEvents(sf::RenderWindow &window);
