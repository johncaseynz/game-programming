#pragma once

#include <SFML\System\Vector3.hpp>

namespace vm
{
	template <class T>
	float inline magnitude(const sf::Vector3<T> &vec)
	{
		return std::sqrt(vec.x * vec.x + vec.y * vec.y + vec.z * vec.z);
	};

	template <class T>
	sf::Vector3<T> normalize(const sf::Vector3<T> &vec)
	{
		float m = magnitude(vec);

		return vec / m;
	};

	template <class T>
	sf::Vector3<T> cross(const sf::Vector3<T> &a, const sf::Vector3<T> &b)
	{
		return sf::Vector3f((a.y * b.z) - (a.z * b.y), (a.z*b.x) - (a.x*b.z), (a.x*b.y) - (a.y*b.x));
	};

	template <class T>
	float product(sf::Vector3<T> a, sf::Vector3<T> b)
	{
		return a.x * b.x + a.y * b.y + a.z * b.z;
	}

	template <class T>
	sf::Vector3<T> project(sf::Vector3<T> vectora, sf::Vector3<T> vectorb)
	{
		sf::Vector3f unitb = vm::normalize(vectorb); // calculate the unit vector of vectorb

		float scale = vm::product(vectora, unitb); // calculate the scale factor for our vector projection using the dot-product

		sf::Vector3f projection = scale * unitb; // scale the unit vector by the scale factor that we have calculated

		return projection;
	}

}