#include "Particle.h"

Particle::Particle(const sf::FloatRect &boundary)
{
	location = sf::Vector2f(0, 0);
	velocity = sf::Vector2f(1, 1);
	acceleration = sf::Vector2f(1, 1);

	this->boundary = boundary;
}

Particle::Particle(const sf::FloatRect &boundary, const  sf::Vector2f &location,const  float &mass)
{
	this->location = location;
	this->velocity = sf::Vector2f();
	this->acceleration = sf::Vector2f();
	this->boundary = boundary;
	this->mass = mass;
}

void Particle::setVelocity(const sf::Vector2f &velocity)
{
	this->velocity = velocity;
}

void Particle::setAcceleration(const sf::Vector2f &acceleration)
{
	this->acceleration = acceleration;
}

sf::Vector2f Particle::getLocation()
{
	return location;
}

void Particle::applyForce(const sf::Vector2f &force)
{
	acceleration = acceleration + (force / mass);
}

void Particle::update(const sf::Time &deltaTime)
{
	velocity = velocity + (acceleration * deltaTime.asSeconds());
	location = location + (velocity * deltaTime.asSeconds());
}

void Particle::checkEdges()
{
	if ((location.x > boundary.width) || (location.x < 0)) {
		velocity.x = velocity.x * -1;
	}
	if ((location.y > boundary.height) || (location.y < 0)) {
		velocity.y = velocity.y * -1;
	}

//	acceleration = sf::Vector2f();

}

Particle::~Particle()
{
}
