#pragma once

#include <SFML\System\Vector2.hpp>

namespace vm
{
	//Get the magnitude of a vector

	template <class T>
	float inline magnitude(const sf::Vector2<T> &vec)
	{
		return std::sqrt(vec.x * vec.x + vec.y * vec.y);
	};


	template <class T>
	sf::Vector2<T> normalize(const sf::Vector2<T> &vec)
	{
		float m = magnitude(vec);

		return vec / m;
	};
}