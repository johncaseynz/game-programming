#pragma once

void bind_texture();

void setup_projection();

void setup_lighting();

void process_events(sf::RenderWindow &window, float &angleX, float &angleY, float &z);

void setup_material();

void draw_square();
