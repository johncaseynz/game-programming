#include "SFML/Graphics.hpp"
#include "SFML/OpenGL.hpp"
#include <iostream>
#include "main.h"
#include <SFML\System\Vector3.hpp>
#include "VectorMath.h"

void setup()
{

}

int main()
{
	sf::RenderWindow window(sf::VideoMode(800, 600), "OpenGL Lighting");

	setup_projection();
	bind_texture();
	setup_lighting();

	float z=-5;
	float angleX = 1;
	float angleY = 1;

	while (window.isOpen())
	{
		process_events(window, angleX,angleY, z);

		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity(); // reset

		glTranslatef(0.f, 0.f, z);

		glRotatef(angleX * 20, 0.0f, 1.0f, 0.f); // rotate around the y-axis
		glRotatef(angleY * 20, 1.0f, 0.0f, 0.f); // rotate around the y-axis

		setup_material();

		draw_square();
		window.display();
	}

	return EXIT_SUCCESS;
}

void draw_square()
{
	//algorithm to draw a large square composed of a lot of little squares
	glBegin(GL_QUADS);

	glNormal3f(0.0f, 0.0f, 1.0f); // point out of the screen

	// the z coordinate of our square
	const float Z = 0.0;

	// max / min extents of our square
	int MAX = 50;
	int MIN = -50;

	// the range of the square
	float RANGE = MAX - MIN;

	// a small factor used for the height / width of the sides of the sub-squares that make up our larger square
	int INDEX_STEPPING = 4; // this factor must divide the RANGE evenly

	// a small factor used to increment the height / width of our texture co-ordinates
	float TOTAL = INDEX_STEPPING / RANGE;

	for (int i = MIN; i < MAX; i += INDEX_STEPPING)
	{
		for (int j = MIN; j < MAX; j += INDEX_STEPPING)
		{
			// calculate the UV co-ordinates for this square
			float u1 = (j + MAX) / RANGE;
			float v1 = (i + MAX) / RANGE;

			float u2 = u1 + TOTAL;
			float v2 = v1 + TOTAL;

			// generate the vectors for the four corners of the quad
			sf::Vector3f a(j, i, Z);
			sf::Vector3f b((j+INDEX_STEPPING), i, Z);
			sf::Vector3f c((j + INDEX_STEPPING), (i + INDEX_STEPPING), Z);
			sf::Vector3f d(j, (i + INDEX_STEPPING), Z);

			sf::Vector3f normal = vm::cross(a - b, a - d); // as the quad is flat just use the normal of the first triangle
			normal = vm::normalize(normal);

			glNormal3f(normal.x, normal.y, normal.z);

			// generate the texture and vertex components of the quad
			glTexCoord2f(u1, v1);
			glVertex3f(j, i, Z);

			glTexCoord2f(u2, v1);
			glVertex3f((j + INDEX_STEPPING), i, Z);

			glTexCoord2f(u2, v2);
			glVertex3f((j + INDEX_STEPPING), (i + INDEX_STEPPING), Z);

			glTexCoord2f(u1, v2);
			glVertex3f(j, (i + INDEX_STEPPING), Z);

			//std::cout << normal.x << ","
			//	<< normal.y << ","
			//	<< normal.z << ","
			//	<< std::endl;
		}
	}
	glEnd();
}

void setup_material()
{
	GLfloat qaBlack[] = { 0.0, 0.0, 0.0, 1.0 };
	GLfloat qaGreen[] = { 0.3, 0.3, 0.3, 1.0 };
	GLfloat qaWhite[] = { 1.0, 1.0, 1.0, 1.0 };
	glMaterialfv(GL_FRONT, GL_AMBIENT, qaGreen);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, qaGreen);
	glMaterialfv(GL_FRONT, GL_SPECULAR, qaWhite);
	glMaterialf(GL_FRONT, GL_SHININESS, 60.0);
}

void process_events(sf::RenderWindow &window, float &angleX, float &angleY, float &z)
{
	sf::Event Event;
	while (window.pollEvent(Event))
	{
		// Close window : exit
		if (Event.type == sf::Event::Closed)
		{
			window.close();
		}
		// Escape key : exit
		if ((Event.type == sf::Event::KeyPressed) && (Event.key.code == sf::Keyboard::Escape))
		{
			window.close();
		}

		if ((Event.key.code == sf::Keyboard::Left)) {
			angleX = angleX + 0.1f;
		}

		if ((Event.key.code == sf::Keyboard::Right)) {
			angleX = angleX - 0.1f;
		}

		if ((Event.key.code == sf::Keyboard::Up)) {
			angleY = angleY + 0.1f;
		}

		if ((Event.key.code == sf::Keyboard::Down)) {
			angleY = angleY - 0.1f;
		}

		if (Event.type == sf::Event::MouseWheelMoved)
		{
			z = z + (Event.mouseWheel.delta*2.0f);
		}
	}
}

void setup_lighting()
{
	// enable lighting effects
	glLightModeli(GL_LIGHT_MODEL_LOCAL_VIEWER, GL_TRUE);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);

	GLfloat LIGHT_POSITION[] = { 0.0f, 10.0f, 1.0f, 1.0f }; // x,y,z,w positional light
	glLightfv(GL_LIGHT0, GL_POSITION, LIGHT_POSITION);

	GLfloat qaAmbientLight[] = { 0.2, 0.2, 0.2, 1.0 };
	GLfloat qaDiffuseLight[] = { 0.8, 0.8, 0.8, 1.0 };
	GLfloat qaSpecularLight[] = { 1.0, 0.9, 0.9, 1.0 };
	glLightfv(GL_LIGHT0, GL_AMBIENT, qaAmbientLight);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, qaDiffuseLight);
	glLightfv(GL_LIGHT0, GL_SPECULAR, qaSpecularLight);
}

void setup_projection()
{
	glMatrixMode(GL_PROJECTION); // Select the builtin projection matrix
	glLoadIdentity();  // reset the projection matrix by loading the projection identity matrix

	GLdouble fovY = 90;
	GLdouble aspect = 1.0f;
	GLdouble zNear = 1.0f;
	GLdouble zFar = 3000.0f;

	const GLdouble pi = 3.1415926535897932384626433832795;
	GLdouble fW, fH;

	fH = tan(fovY / 360 * pi) * zNear;
	fW = fH * aspect;

	// define a perspective projection
	glFrustum(-fW, fW, -fH, fH, zNear, zFar); // multiply the set matrix; by a perspective matrix
}

void bind_texture()
{
	sf::Image texture;
	texture.loadFromFile("glass.bmp");
	GLuint texture_handle;
	glGenTextures(1, &texture_handle); // allocate a texture handle within OpenGL
	glBindTexture(GL_TEXTURE_2D, texture_handle); // bind to the texture handle target

												  // import texture into OpenGL
	glTexImage2D(
		GL_TEXTURE_2D, 0, GL_RGBA,
		texture.getSize().x, texture.getSize().y,
		0,
		GL_RGBA, GL_UNSIGNED_BYTE, texture.getPixelsPtr()
	);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glEnable(GL_TEXTURE_2D); // enable 2D textures
}
