#include <iostream>
#include <SFML/Graphics.hpp>
#include <random>

std::default_random_engine generator;
std::uniform_int_distribution<int> randomX(0, 800);
std::uniform_int_distribution<int> randomY(0, 600);
std::uniform_int_distribution<int> direction(0, 1);

class Ball
{
	private:
		float deltaX = 1.0f;
		float deltaY = 1.0f;

		float x1, x2, y1, y2;

		sf::CircleShape circle;
	public:

		Ball()
		{
			circle.setFillColor(sf::Color::Red);
			circle.setPosition(randomX(generator), randomY(generator));
			circle.setRadius(20);

			this->x1 = 0;
			this->x2 = 800;
			this->y1 = 0;
			this->y2 = 600;

			if (direction(generator))
			{
				deltaX = 1.0f;
			}
			else
			{
				deltaX = -1.0f;
			}
			if (direction(generator))
			{
				deltaY = 1.0f;
			}
			else
			{
				deltaY = -1.0f;
			}
		}

		Ball(const float x1, const float y1, const float x2, const float y2) : Ball()
		{
			this->x1 = x1;
			this->x2 = x2;
			this->y1 = y1;
			this->y2 = y2;
		}

		void update()
		{
			if (circle.getPosition().y > y2)
			{
				deltaY = deltaY *-1.0f;
			}
			if (circle.getPosition().y < y1)
			{
				deltaY = deltaY *-1.0f;
			}
			if (circle.getPosition().x > x2)
			{
				deltaX = deltaX * -1.0f;
			}
			if (circle.getPosition().x < x1)
			{
				deltaX = deltaX * -1.0f;
			}

			circle.move(deltaX, deltaY);
		}
		void paint(sf::RenderWindow *window)
		{
			window->draw(circle);
		}
};

int main(int argc, int** argv)
{
	std::cout << "First SFML Program" << std::endl;

	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;

	sf::RenderWindow window(sf::VideoMode(800, 600), "Bouncing Balls", sf::Style::Default, settings);
	//window.setFramerateLimit(60); // control how fast the screen is refreshed (fps)

	const int BALL_SZ = 200;
	Ball ball[BALL_SZ];

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}
		}
		window.clear();

		for (int i = 0; i < BALL_SZ; i++)
		{
			ball[i].update();
			ball[i].paint(&window);
		}

		window.display();
	}
	return 0;
}