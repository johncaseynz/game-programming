#include "SFML/Graphics.hpp"
#include "SFML/OpenGL.hpp" // SFML OpenGL include
#include <iostream>

int main()
{
	sf::ContextSettings settings;
	settings.depthBits = 24;          // Request a 24-bit depth buffer
	settings.stencilBits = 8;           // Request a 8 bits stencil buffer
	settings.antialiasingLevel = 2; // Request 2 levels of antialiasing

	// Use SFML to handle the window for us
	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML OpenGL Triangle", sf::Style::Close, settings);

	//prepare the OpenGL surface for drawing
	glClearColor(0.3f, 0.3f, 0.3f, 0.f); // set the background colour for when we clear the screen RGBA values in the 0.0 to 1.0 range. This gives us a nice grey background.


	glMatrixMode(GL_PROJECTION); // Select the builtin projection matrix
	glLoadIdentity();  // reset the projection matrix by loading the projection identity matrix

	GLdouble fovY = 90;
	GLdouble aspect = 1.0f;
	GLdouble zNear = 1.0f;
	GLdouble zFar = 300.0f;

	const GLdouble pi = 3.1415926535897932384626433832795;
	GLdouble fW, fH;

	fH = tan(fovY / 360 * pi) * zNear;
	fW = fH * aspect;

	// define a perspective projection
	glFrustum(-fW, fW, -fH, fH, zNear, zFar); // multiply the set matrix; by a perspective matrix

	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LESS);
	glFrontFace(GL_CCW);
	glEnable(GL_DEPTH_TEST);

	float angle = 0;

	// Start game loop
	while (window.isOpen())
	{
		// Process events
		sf::Event Event;
		while (window.pollEvent(Event))
		{
			// Close window : exit
			if (Event.type == sf::Event::Closed)
				window.close();

			// Escape key : exit
			if ((Event.type == sf::Event::KeyPressed) && (Event.key.code == sf::Keyboard::Escape))
				window.close();
		}

		//Prepare for drawing
		// Clear color and depth buffer
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity(); // reset
		glTranslatef(0.f, 0.f, -2); // position the  model at z-position -2; ie. away from us

		//glRotatef(angle+=0.05f, 1.f, 0.f, 0.f);
		glRotatef(angle += 0.05f, 0.f, 1.f, 0.f);

		//Draw a 2D Red Triangle
		glBegin(GL_QUADS);
			// define the colour
			glColor3f(1.0f, 0.0f, 0.0f);
			
			// top
			glVertex3f(0.5f,0.5f,0.5f); // front right top
			glVertex3f(0.5f, 0.5f, -0.5f); // back right top
			glVertex3f(-0.5f, 0.5f, -0.5f); // back left top
			glVertex3f(-0.5f, 0.5f, 0.5f); // front left top

			glColor3f(0.0f, 1.0f, 0.0f);
			// back
			glVertex3f(0.5f, 0.5f, -0.5f); // back right top
			glVertex3f(-0.5f, 0.5f, -0.5f); // back left top
			glVertex3f(-0.5f, -0.5f, -0.5f); // back left bottom
			glVertex3f(0.5f, -0.5f, -0.5f); // back right bottom

			glColor3f(0.0f, 0.0f, 1.0f);
			// left side
			glVertex3f(-0.5f, 0.5f, -0.5f); // side left top
			glVertex3f(-0.5f, -0.5f, -0.5f); // side left bottom
			glVertex3f(-0.5f, -0.5f, 0.5f); // side left front
			glVertex3f(-0.5f, 0.5f, 0.5f); // side left top

			glColor3f(1.0f, 0.0f, 1.0f);
			// right side
			glVertex3f(0.5f, 0.5f, -0.5f); // side left top
			glVertex3f(0.5f, -0.5f, -0.5f); // side left bottom
			glVertex3f(0.5f, -0.5f, 0.5f); // side left front
			glVertex3f(0.5f, 0.5f, 0.5f); // side left top

			glColor3f(1.0f, 1.0f, 0.0f);
			// front
			glVertex3f(0.5f, 0.5f, 0.5f); // back right top
			glVertex3f(-0.5f, 0.5f, 0.5f); // back left top
			glVertex3f(-0.5f, -0.5f, 0.5f); // back left bottom
			glVertex3f(0.5f, -0.5f, 0.5f); // back right bottom

			glColor3f(0.0f, 1.0f, 1.0f);
			// bottom
			glVertex3f(0.5f, -0.5f, 0.5f); // front right top
			glVertex3f(0.5f, -0.5f, -0.5f); // back right top
			glVertex3f(-0.5f,-0.5f, -0.5f); // back left top
			glVertex3f(-0.5f, -0.5f, 0.5f); // front left top

		glEnd();

		// Use the SFML window routines to display the OpenGL triangle
		window.display();
	}

	return EXIT_SUCCESS;
}