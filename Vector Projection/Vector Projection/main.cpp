#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include <iostream>
#include <random>
#include <list>
#include <cmath>

#include "VectorMath.h"

const int WIDTH = 800;
const int HEIGHT = 600;

void processEvents(sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
	}
}

float cartesianXToScreenX(float cartesianX, float width)
{
	return cartesianX + (width / 2.0f);
}

float cartesianYToScreenY(float cartesianY, float height)
{
	return (height / 2) - cartesianY;
}

sf::Vector2f worldToScreen(sf::Vector2f world, float width, float height)
{
	return sf::Vector2f(cartesianXToScreenX(world.x, width), cartesianYToScreenY(world.y, height));
}

void drawVector(sf::RenderWindow &window, sf::Vector2f vector, float width, float height, sf::Color color)
{
	sf::Vertex vertices[2] =
	{
		sf::Vertex(sf::Vector2f(cartesianXToScreenX(0,WIDTH), cartesianYToScreenY(0,HEIGHT)),color),
		sf::Vertex(worldToScreen(vector,WIDTH,HEIGHT),color)
	};
	window.draw(vertices, 2, sf::Lines);
}

int main()
{
	sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "SFML Vector Projection");
	window.setFramerateLimit(60);

	while (window.isOpen())
	{
		processEvents(window);

		window.clear();

		sf::Vector2f vectora(50, 200);
		sf::Vector2f vectorb(200, 60);

		drawVector(window, vectora, WIDTH, HEIGHT,sf::Color::Red);
		drawVector(window, vectorb, WIDTH, HEIGHT, sf::Color::Green);
		
		sf::Vector2f unitb = vm::normalize(vectorb);
		float product = vm::product(vectora, unitb);
		sf::Vector2f projected = product * unitb;

		drawVector(window, unitb, WIDTH, HEIGHT, sf::Color::Cyan);
		drawVector(window, projected, WIDTH, HEIGHT, sf::Color::Blue);

		window.display();
	}
}

