#include "Particle.h"

Particle::Particle(const sf::FloatRect &boundary, const  sf::Vector2f &location)
{
	this->current = location;
	this->previous = location -sf::Vector2f(0.05, 0.05);
	this->acceleration = sf::Vector2f();
	this->boundary = boundary;
}

void Particle::setAcceleration(const sf::Vector2f &acceleration)
{
	this->acceleration = acceleration;
}

sf::Vector2f Particle::getAcceleration()
{
	return acceleration;
}

sf::Vector2f& Particle::getCurrentPosition()
{
	return current;
}

void Particle::update(const sf::Time &deltaTime)
{
	float time = deltaTime.asSeconds();

	sf::Vector2f velocity = current - previous;

	previous = current;
	current = current + velocity + (acceleration * (time * time));
}

void Particle::move(float offsetX, float offsetY)
{
	current.x = current.x - offsetX;
	current.y = current.y - offsetY;
}

void Particle::checkEdges()
{
	// reverse the direction

	sf::Vector2f velocity = current - previous;

	if ((current.x > boundary.width))
	{
		current.x = boundary.width;
		previous.x = current.x + velocity.x;
	}

	if ((current.x < 0))
	{
		current.x = 0;
		previous.x = current.x + velocity.x;

	}

	if ((current.y > boundary.height))
	{
		current.y = boundary.height;
		previous.y = current.y + velocity.y;
	}

	if (current.y < 0)
	{
		current.y = 0;
		previous.y = current.y + velocity.y;
	}
}

void Particle::draw(sf::RenderWindow & window)
{
	circle.setOrigin(sf::Vector2f(2.0f, 2.0f));
	circle.setRadius(2);
	circle.setFillColor(sf::Color::Green);
	circle.setPosition(current);

	window.draw(circle);
}

Particle::~Particle()
{
}