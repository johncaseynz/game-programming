#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <random>
#include <list>
#include <cmath>

#include "Particle.h"
#include "VectorMath.h"
#include "Constraint.h"


const int WIDTH = 1024;
const int HEIGHT = 768;

void processEvents(sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
	}
}

int main()
{
	sf::Time TimePerFrame = sf::seconds(1.0f / 120);

	sf::ContextSettings settings;
	settings.antialiasingLevel = 8;

	sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "SFML Verlet Collision Detection", sf::Style::Default, settings);
	
	std::list <Constraint*> constraints;
	std::list <Particle*> particles;

	const sf::FloatRect boundary = sf::FloatRect(sf::Vector2f(0, 0), sf::Vector2f(window.getSize()));

	float left = 200;
	float top = 200;

	float height = 50;
	float width = 50;

	Particle* leftTop = new Particle(boundary, sf::Vector2f(left, top));
	Particle* rightTop = new Particle(boundary, sf::Vector2f(left + width, top));
	Particle* leftBottom = new Particle(boundary, sf::Vector2f(left, top + height));
	Particle* rightBottom = new Particle(boundary, sf::Vector2f(left + width, top + height));
	
	leftTop->setAcceleration(sf::Vector2f(0,60.0f));

	particles.push_back(leftTop);
	particles.push_back(rightTop);
	particles.push_back(leftBottom);
	particles.push_back(rightBottom);

	// add a constraint between all of the different sides of the box
	constraints.push_back(new Constraint(leftTop, rightTop,width));
	constraints.push_back(new Constraint(rightTop, rightBottom, height));
	constraints.push_back(new Constraint(rightBottom,leftBottom, width));
	constraints.push_back(new Constraint(leftBottom, leftTop, height));

	// add a strut between the top right and bottom left
	constraints.push_back(new Constraint(rightTop, leftBottom,sqrt(width * width + height * height)));
	
	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;
	while (window.isOpen())
	{
		processEvents(window);

		timeSinceLastUpdate += clock.restart();

		while (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;
			processEvents(window);

			for(std::list<Particle*>::iterator it = particles.begin(); it != particles.end(); ++it)
			{
				(*it)->checkEdges();
				(*it)->update(TimePerFrame);
			}

			for (std::list<Constraint*>::iterator it = constraints.begin(); it != constraints.end(); ++it)
			{
				(*it)->update();
			}
		}
		window.clear();

		// draw the constraints
		for (std::list<Constraint*>::iterator it = constraints.begin(); it != constraints.end(); ++it)
		{
			(*it)->draw(window);
		}

		// draw particles
		for (std::list<Particle*>::iterator it = particles.begin(); it != particles.end(); ++it)
		{
			(*it)->draw(window);
		}

		window.display();
	}
}

