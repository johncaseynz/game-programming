#include "Constraint.h"

Constraint::Constraint(Particle *particle1, Particle *particle2, float length) : particle1(particle1), particle2(particle2)
{
	this->restlength = length;
}


Constraint::~Constraint()
{
}


Particle* Constraint::getParticle1()
{
	return this->particle1;
}

Particle* Constraint::getParticle2()
{
	return this->particle2;
}

void Constraint::update()
{
	sf::Vector2f distance = particle2->getCurrentPosition() - particle1->getCurrentPosition();
	float deltalength = vm::magnitude(distance);

	float difference = restlength - deltalength;
	float percent = difference / deltalength / 2;

	float offsetX = distance.x * percent;
	float offsetY = distance.y * percent;

	particle1->move(offsetX, offsetY);
	particle2->move(-offsetX, -offsetY);
}

void Constraint::draw(sf::RenderWindow & window)
{
	sf::Vertex vertices[2] =
	{
		sf::Vertex(particle1->getCurrentPosition(),sf::Color::Red),
		sf::Vertex(particle2->getCurrentPosition(),sf::Color::Red)
	};

	window.draw(vertices, 2, sf::Lines);
}
