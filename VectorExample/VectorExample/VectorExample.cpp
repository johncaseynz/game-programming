#include <SFML/Graphics.hpp>

int main()
{
	// create the window
	sf::RenderWindow window(sf::VideoMode(800, 600), "My window");
	window.setFramerateLimit(60);

	sf::CircleShape circle(20);
	circle.setFillColor(sf::Color::Green);

	// position
	float x = 0;
	float y = 200;

	// velocity
	float vx = 0;
	float vy = 0;

	// acceleration
	float ax = 0.01; 
	float ay = 0.01;

	// run the program as long as the window is open
	while (window.isOpen())
	{
		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		while (window.pollEvent(event))
		{
			// "close requested" event: we close the window
			if (event.type == sf::Event::Closed)
				window.close();
		}

		circle.setPosition(sf::Vector2f(x, y));

		// clear the window with black color
		window.clear(sf::Color::Black);

		// draw everything here...
		window.draw(circle);

		// end the current frame
		window.display();

		vx = vx + ax;
		vy = vy + ay;

		x = x + vx;
		y = y + vy;

		if (x > window.getSize().x)
		{
			ax = ax * -1;
			vx = vx * -1;
		}

		if (y > window.getSize().y)
		{
			ay = ay * -1;
			vy = vy * -1;
		}

		if (y < 0)
		{
			ay = ay * -1;
			vy = vy * -1;
		}

		if (x < 0)
		{
			ax = ax * -1;
			vx = vx * -1;
		}
	}

	return 0;
}