#define _USE_MATH_DEFINES
#include <cmath>

#include <SFML/Graphics.hpp>
#include <iostream>
#include <cmath>


int main()
{
	// create the window
	sf::RenderWindow window(sf::VideoMode(800, 600), "My window");
	window.setFramerateLimit(30);


	sf::Transform scale(
		8, 0, 0,
		0, 8, 0,
		0, 0, 1
	);

	sf::Transform transform(
		1, 0, window.getSize().x / 2,
		0, 1, window.getSize().y / 2,
		0, 0, 1
	);

	sf::Transform shear(
		1, 1, 0,
		1, 1, 0,
		0, 0, 1
	);

	//t.scale(sf::Vector2f(2, 2)).translate(0,0);

	sf::Sprite sprite;

	//sprite.setPosition(sf::Vector2f(window.getSize().x / 2, window.getSize().y / 2));

	sf::Texture down;
	if (!down.loadFromFile("assets/hero_down.png"))
	{
		std::cout << "Error loading resource hero_down.png" << std::endl;
	}

	sf::Texture walk_down;
	if (!walk_down.loadFromFile("assets/hero_walk_down.png"))
	{
		std::cout << "Error loading resource hero_down.png" << std::endl;
	}

	sf::Texture walk_up;
	if (!walk_up.loadFromFile("assets/hero_walk_up.png"))
	{
		std::cout << "Error loading resource hero_walk_up.png" << std::endl;
	}

	sf::Texture walk_left;
	if (!walk_left.loadFromFile("assets/hero_walk_left.png"))
	{
		std::cout << "Error loading resource hero_walk_up.png" << std::endl;
	}

	sf::Texture walk_right;
	if (!walk_right.loadFromFile("assets/hero_walk_right.png"))
	{
		std::cout << "Error loading resource hero_walk_up.png" << std::endl;
	}

	sprite.setTexture(down);

	float angle = (M_PI / 180) * 30;

	// run the program as long as the window is open
	while (window.isOpen())
	{
		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		while (window.pollEvent(event))
		{
			// "close requested" event: we close the window
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			{
				angle = angle - 0.1;
			}

			if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			{
				angle = angle + 0.1;
			}

		}

		std::cout << "angle:" << angle << std::endl;

		sf::Transform rotate(
			std::cos(angle), -std::sin(angle), 0,
			std::sin(angle), std::cos(angle), 0,
			0, 0, 1
		);



		// clear the window with black color
		window.clear(sf::Color::Black);
		// Scale, Rotate, Transform using post multiply to keep OpenGL happy
		sf::Transform finalTransform = transform * scale; // transform * rotate * scale;
		const float* matrix = finalTransform.getMatrix();
		
		for (int i = 0;i < 16; i++)
		{
			std::cout << matrix[i];
			
			if ((i + 1) % 4 == 0)
			{
				std::cout << std::endl;
			}
			else
			{
				std::cout << ",";
			}
			
		}

		// draw everything here...
		window.draw(sprite, finalTransform);

		// end the current frame
		window.display();
	}

	return 0;
}