#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include <iostream>
#include <random>
#include <list>
#include <cmath>

#include "Particle.h"
#include "VectorMath.h"

const int PARTICLE_SZ = 1;
const int PARTICLE_RADIUS = 8;

std::default_random_engine generator;
std::uniform_real_distribution<float> vx(-5, +5);
std::uniform_real_distribution<float> vy(-150,-50);

std::uniform_real_distribution<float> x(-100, +100);


std::uniform_real_distribution<float> velocity(150, 450);
std::uniform_int_distribution<int> color(0, 255);

void createParticle(std::list <Particle> &particles, const sf::FloatRect &rect, sf::RenderWindow &window, sf::Vector2f &location)
{
	sf::Color random = sf::Color(185,61,28); // orange

	for (int i = 0; i < PARTICLE_SZ; i++)
	{
		location.x = location.x + x(generator);
		Particle particle = Particle(rect, location, 1);

		float x = vx(generator);
		float y = vy(generator);

		particle.setVelocity(sf::Vector2f(x, y));

		sf::CircleShape circle = particle.getCircleShape();

		circle.setRadius(PARTICLE_RADIUS);
		circle.setFillColor(random);

		particle.setCircleShape(circle);
		particles.push_front(particle);
	}
}


void processEvents(std::list <Particle> &particles, const sf::FloatRect &rect, sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
	}

}

int main()
{
	bool textures = true;
	bool additive = true;

	float angle = 0;
	sf::Time TimePerFrame = sf::seconds(1.f / 120.f);

	sf::RenderStates state(sf::BlendAdd);

	sf::Texture texture;
	if (!texture.loadFromFile("particleTexture.png"))
	{
		// error...
	}

	sf::RenderWindow window(sf::VideoMode(1024, 768), "SFML Particle Jet Emitter");
	window.setFramerateLimit(120);

	std::list <Particle> particles;

	const sf::FloatRect rect = sf::FloatRect(sf::Vector2f(0, 0), sf::Vector2f(window.getSize()));

	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;
	while (window.isOpen())
	{
		processEvents(particles, rect, window);

		createParticle(particles, rect, window, sf::Vector2f(window.getSize().x / 2, window.getSize().y / 2));

		timeSinceLastUpdate += clock.restart();

		while (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;
			processEvents(particles, rect, window);
			createParticle(particles, rect, window, sf::Vector2f(window.getSize().x / 2, window.getSize().y / 2));

			for(std::list<Particle>::iterator it = particles.begin(); it != particles.end(); ++it)
			{
				it->update(TimePerFrame);
				it->checkEdges();
			}
		}

		window.clear();

		std::cout <<"sz:" << particles.size() << std::endl;

		for (std::list<Particle>::iterator it = particles.begin(); it != particles.end(); ++it)
		{
			sf::CircleShape circle = it->getCircleShape();

			sf::Color random = circle.getFillColor();
			random.a = it->getLife();
			circle.setFillColor(random);

			if (textures)
			{
				sf::Sprite sprite;
				sprite.setTexture(texture);
				sprite.setScale(0.2f, 0.2f);
				sprite.setColor(random);
				sprite.setPosition(circle.getPosition());

				if (additive)
				{
					window.draw(sprite, state);
				}
			}
			else
			{
				if (additive)
				{
					window.draw(circle, state);
				}
				else
				{
					window.draw(circle);
				}
				
			}




		}
		for (std::list<Particle>::iterator it = particles.begin(); it != particles.end();)
		{
			if (it->getLife() < 1)
			{
				it = particles.erase(it);
			}
			else
			{
				++it;
			}
		}
		window.display();
	}
}

