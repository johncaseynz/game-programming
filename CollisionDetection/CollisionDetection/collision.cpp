#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <random>
#include <list>
#include <cmath>

#include "Particle.h"
#include "VectorMath.h"

const int PARTICLE_SZ = 100;
const int PARTICLE_RADIUS = 10;
const int SPEED = 200;

std::default_random_engine generator;
std::uniform_real_distribution<float> vx(-SPEED, +SPEED);
std::uniform_real_distribution<float> vy(-SPEED,+SPEED);

void createParticle(std::list <Particle> &particles, const sf::FloatRect &rect, sf::RenderWindow &window, const sf::Vector2f &location)
{
	sf::Color colour = sf::Color::Green;

	Particle particle = Particle(rect, location, 1);

	float x = vx(generator);
	float y = vy(generator);

	particle.setVelocity(sf::Vector2f(x, y));

	sf::CircleShape circle = particle.getCircleShape();

	circle.setRadius(PARTICLE_RADIUS);
	circle.setFillColor(colour);

	particle.setCircleShape(circle);
	particles.push_front(particle);
	
}


void processEvents(std::list <Particle> &particles, const sf::FloatRect &rect, sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
	}

}

int main()
{
	float angle = 0;
	sf::Time TimePerFrame = sf::seconds(1.f / 120.f);

	
	sf::RenderWindow window(sf::VideoMode(1024, 768), "SFML Collision Detection");
	window.setFramerateLimit(120);

	std::list <Particle> particles;

	const sf::FloatRect rect = sf::FloatRect(sf::Vector2f(0, 0), sf::Vector2f(window.getSize()));

	for (int i = 0; i < PARTICLE_SZ; i++)
	{
		createParticle(particles, rect, window, sf::Vector2f(window.getSize().x / 2, window.getSize().y / 2));
	}

	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;
	while (window.isOpen())
	{
		processEvents(particles, rect, window);

		timeSinceLastUpdate += clock.restart();

		while (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;
			processEvents(particles, rect, window);
		
			for(std::list<Particle>::iterator it = particles.begin(); it != particles.end(); ++it)
			{
				it->update(TimePerFrame);
				it->checkEdges();
				it->checkCollision(particles);

			}
		}

		window.clear();

		for (std::list<Particle>::iterator it = particles.begin(); it != particles.end(); ++it)
		{
			sf::CircleShape circle = it->getCircleShape();
			window.draw(circle);
		}
		
		window.display();
	}
}

