#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <random>
#include <list>
#include <cmath>

#include "Particle.h"
#include "VectorMath.h"

const int PARTICLE_SZ = 2;
const int PARTICLE_RADIUS = 2;

std::default_random_engine generator;
std::uniform_real_distribution<float> vx(-5, +5);
std::uniform_real_distribution<float> vy(-150,-50);

std::uniform_real_distribution<float> velocity(150, 450);
std::uniform_int_distribution<int> color(0, 255);

void createParticle(std::list <Particle> &particles, const sf::FloatRect &rect, sf::RenderWindow &window, const sf::Vector2f &location, float angle)
{
	sf::Color random = sf::Color::Red;

	for (int i = 0; i < PARTICLE_SZ; i++)
	{
		Particle particle = Particle(rect, location, 1);

		float xangle = std::cos(angle);
		float yangle = std::sin(angle);

		float x = velocity(generator) * xangle;
		float y = velocity(generator) * yangle;

		particle.setVelocity(sf::Vector2f(x, y));

		sf::CircleShape circle = particle.getCircleShape();

		circle.setRadius(PARTICLE_RADIUS);
		circle.setFillColor(random);

		particle.setCircleShape(circle);
		particles.push_front(particle);
	}
}


void processEvents(std::list <Particle> &particles, const sf::FloatRect &rect, sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
	}

}

int main()
{
	float angle = 0;
	sf::Time TimePerFrame = sf::seconds(1.f / 120.f);

	sf::RenderWindow window(sf::VideoMode(1024, 768), "SFML Particle Twirler");
	window.setFramerateLimit(120);

	std::list <Particle> particles;

	const sf::FloatRect rect = sf::FloatRect(sf::Vector2f(0, 0), sf::Vector2f(window.getSize()));

	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;
	while (window.isOpen())
	{
		processEvents(particles, rect, window);

		createParticle(particles, rect, window, sf::Vector2f(window.getSize().x / 2, window.getSize().y / 2),angle);

		timeSinceLastUpdate += clock.restart();

		while (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;
			processEvents(particles, rect, window);
			createParticle(particles, rect, window, sf::Vector2f(window.getSize().x / 2, window.getSize().y / 2), angle);

			for(std::list<Particle>::iterator it = particles.begin(); it != particles.end(); ++it)
			{
				it->update(TimePerFrame);
				it->checkEdges();
			}
		}

		window.clear();

		std::cout <<"sz:" << particles.size() << std::endl;

		for (std::list<Particle>::iterator it = particles.begin(); it != particles.end(); ++it)
		{
			sf::CircleShape circle = it->getCircleShape();

			sf::Color random = circle.getFillColor();
			random.a = it->getLife();
			circle.setFillColor(random);

			window.draw(circle);

		}
		for (std::list<Particle>::iterator it = particles.begin(); it != particles.end();)
		{
			if (it->getLife() < 1)
			{
				it = particles.erase(it);
			}
			else
			{
				++it;
			}
		}
		window.display();

		angle+=0.05; // = angle++; //+ 0.001;
	}
}

