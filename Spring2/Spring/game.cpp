#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>
#include <iostream>
#include <random>
#include <list>
#include <cmath>


#include "Particle.h"
#include "Spring.h"
#include "VectorMath.h"

void processEvents(Spring &spring, const sf::FloatRect &rect, sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
	}
}

int main()
{
	sf::Time TimePerFrame = sf::seconds(1.f / 60);

	sf::ContextSettings context;
	context.antialiasingLevel = 10;

	sf::RenderWindow window(sf::VideoMode(1024, 768), "SFML Springs", sf::Style::Default, context);

	const sf::FloatRect rect = sf::FloatRect(sf::Vector2f(0, 0), sf::Vector2f(window.getSize()));

	sf::Clock clock;
	sf::Time timeSinceLastUpdate = sf::Time::Zero;

	Particle particlea = Particle(rect, sf::Vector2f(window.getSize().x / 2, (window.getSize().y / 2)), 1);
	Particle particleb = Particle(rect, sf::Vector2f(window.getSize().x / 2 - 300, (window.getSize().y / 2) + 300), 1);
	Particle particlec = Particle(rect, sf::Vector2f(window.getSize().x / 2 + 300, (window.getSize().y / 2) + 300), 1);

	Spring springa = Spring(particlea, particleb, 200);
	Spring springb = Spring(particleb, particlec, 200);
	Spring springc = Spring(particlec, particlea, 200);

	while (window.isOpen())
	{
		processEvents(springa, rect, window);

		timeSinceLastUpdate += clock.restart();

		while (timeSinceLastUpdate > TimePerFrame)
		{
			timeSinceLastUpdate -= TimePerFrame;
			processEvents(springa, rect, window);

			//for (int i = 0; i < 20; i++)
			{
				springa.update(TimePerFrame);
				springb.update(TimePerFrame);
				springc.update(TimePerFrame);
			}
		}

		window.clear();
		sf::CircleShape origin = sf::CircleShape(10);
		origin.setOrigin(10, 10);
		origin.setPosition(springa.getOrigin().getLocation());
		origin.setFillColor(sf::Color::Red);

		window.draw(origin);

		sf::CircleShape weighta = sf::CircleShape(10);
		weighta.setOrigin(10, 10);
		weighta.setPosition(springb.getOrigin().getLocation());
		weighta.setFillColor(sf::Color::Green);

		sf::CircleShape weightb = sf::CircleShape(10);
		weightb.setOrigin(10, 10);
		weightb.setPosition(springc.getOrigin().getLocation());
		weightb.setFillColor(sf::Color::Blue);

		sf::Vertex verticesAB[2] =
		{
			sf::Vertex(springa.getOrigin().getLocation()),
			sf::Vertex(springb.getOrigin().getLocation())
		};

		sf::Vertex verticesBC[2] =
		{
			sf::Vertex(springb.getOrigin().getLocation()),
			sf::Vertex(springc.getOrigin().getLocation())
		};

		sf::Vertex verticesCA[2] =
		{
			sf::Vertex(springc.getOrigin().getLocation()),
			sf::Vertex(springa.getOrigin().getLocation())
		};

		window.draw(verticesAB, 2, sf::Lines);
		window.draw(verticesBC, 2, sf::Lines);
		window.draw(verticesCA, 2, sf::Lines);

		window.draw(weighta);
		window.draw(weightb);

		window.display();
	}
}

