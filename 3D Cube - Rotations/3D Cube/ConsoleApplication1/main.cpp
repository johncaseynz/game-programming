#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include <iostream>
#include <random>
#include <list>
#include <cmath>

float SCALE = 400;
float DISTANCE = 3.0f;

const int POINT_SZ = 8;
const int FACE_SZ = 4;

const int WIDTH = 800;
const int HEIGHT = 600;

float angleX = 0;
float angleY = 0;
float angleZ = 0;

void processEvents(sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
	}
}

float cartesianXToScreenX(float cartesianX, float width)
{
	return cartesianX+(width / 2.0f);
}

float cartesianYToScreenY(float cartesianY, float height)
{
	return (height / 2) - cartesianY;
}

float project(float world_point, float z)
{
	return world_point / (z + DISTANCE);
}

sf::Vector2f convert_point(sf::Vector3f point)
{
	float x = project(point.x, point.z) * SCALE;
	float y = project(point.y, point.z) * SCALE;

	float translateX = cartesianXToScreenX(x, WIDTH);
	float translateY = cartesianYToScreenY(y, HEIGHT);
	
	sf::Vector2f pnt;
	pnt.x = translateX;
	pnt.y = translateY;

	return pnt;
}


sf::Vector3f rotateX(sf::Vector3f vector)
{
	sf::Vector3f rotateX;

	double cosa = std::cos(angleX);
	double sina = std::sin(angleX);

	rotateX.x = vector.x;
	rotateX.y = (float)(vector.y * cosa - vector.z * sina);
	rotateX.z = (float)(vector.y * sina + vector.z * cosa);

	return rotateX;
}

sf::Vector3f rotateY(sf::Vector3f vector)
{
	sf::Vector3f rotateY;

	double cosa = std::cos(angleY);
	double sina = std::sin(angleY);

	rotateY.x = (float)(vector.z * sina + vector.x * cosa);
	rotateY.y = vector.y;
	rotateY.z = (float)(vector.z * cosa - vector.x * sina);

	return rotateY;
}

sf::Vector3f rotateZ(sf::Vector3f vector)
{
	sf::Vector3f rotated;

	double cosa = std::cos(angleZ);
	double sina = std::sin(angleZ);

	rotated.x = (float)(vector.x * cosa - vector.y * sina);
	rotated.y = (float)(vector.x * sina + vector.y * cosa);
	rotated.z = vector.z;

	return rotated;
}

int main()
{
	sf::Vector3f points[POINT_SZ];

	points[0] = sf::Vector3f(1, 1, 1);
	points[1] = sf::Vector3f(1, 1, -1);
	points[2] = sf::Vector3f(1, -1, -1);
	points[3] = sf::Vector3f(1, -1, 1);
	points[4] = sf::Vector3f(-1, -1, 1);
	points[5] = sf::Vector3f(-1, -1, -1);
	points[6] = sf::Vector3f(-1, 1, -1);
	points[7] = sf::Vector3f(-1, 1, 1);

	int faces[FACE_SZ][FACE_SZ] = {
		{ 0, 1, 2, 3 }, // right
		{ 0, 1, 6, 7 }, // top
		{ 6, 7, 4, 5 }, // left
		{ 4, 5, 2, 3 }  // bottom
	};

	sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "SFML 3D Cube Rotations");
	window.setFramerateLimit(60);

	while (window.isOpen())
	{
		processEvents(window);

		window.clear();

		for (int i = 0; i <8; i++)
		{
			sf::Vector3f world_point = points[i];
			world_point = rotateX(world_point);
			world_point = rotateY(world_point);
			world_point = rotateZ(world_point);
			points[i] = world_point;
		}

		for (int i = 0; i < FACE_SZ; i++)
		{
			sf::VertexArray line (sf::LinesStrip,5);
			
			for (int j = 0; j< FACE_SZ; j++)
			{
				sf::Vector3f world_point = points[faces[i][j]];
				sf::Vector2f screen_point = convert_point(world_point);

				line[j].color = sf::Color::Green;
				line[j].position = screen_point;
			}

			line[4] = line[0]; // close the quad, does not happen automatically
			window.draw(line);
		}

		window.display();

		angleX = angleX + 0.00001f;

		if (angleX == std::_Pi)
		{
			angleX = 0;
		}

		angleY = angleY + 0.00001f;

		if (angleY == std::_Pi)
		{
			angleY = 0;
		}

		angleZ = angleZ + 0.00001f;

		if (angleZ == std::_Pi)
		{
			angleZ = 0;
		}

		// translate all the points
		//for (int i = 0; i < POINT_SZ; i++)
		//{
		//	points[i].z = points[i].z - 0.5;
		//}
	}
}

