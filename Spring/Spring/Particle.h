#include <SFML\System.hpp>
#include <SFML\Graphics.hpp>
#include <SFML\Window.hpp>
#include <SFML\Graphics\Rect.hpp>
#include "VectorMath.h"

#pragma once
class Particle
{
	protected:
		
		sf::Vector2f location; // difference between location and the origin
		sf::Vector2f velocity; // the rate of change in location
		sf::Vector2f acceleration; // the rate of change in velocity
		float mass;
		float topSpeed;
		sf::FloatRect boundary;
		int life = 255;
	public:

		sf::Vector2f getVelocity();
		void setVelocity(const sf::Vector2f &velocity);
		void setAcceleration(const sf::Vector2f &acceleration);
		sf::Vector2f getAcceleration();

		float getMass();
		int getLife();
		void applyForce(const sf::Vector2f &force);

		void setLocation(const sf::Vector2f &location);
		sf::Vector2f getLocation();
		void update(const sf::Time &deltaTime);
		void checkEdges();

		Particle(const sf::FloatRect &boundary, const  sf::Vector2f &location, const float &mass);
		Particle();
		~Particle();
};

