#include "StdAfx.h"
#include "Maze.h"
#include "Game.h"
#include "resource.h"
#include "BackBuffer.h"


CMaze::CMaze(void)
{
}

CMaze::~CMaze(void)
{

}

void CMaze::InitialiseMap()
{
	int iMask = IDB_MAPMASK;
	int iSprite = IDB_MAP;
	CSprite::Initialise(iSprite, iMask);
}

void CMaze::InitialisePill(int iPillSize)
{
	int iSprite = 0;
	int iMask = 0;
	if(iPillSize == 0)
	{
		iSprite = IDB_SMALLPILL;
		iMask	= IDB_SMALLPILLMASK;
	}
	else if (iPillSize == 1)
	{
		iSprite = IDB_BIGPILL;
		iMask	= IDB_BIGPILLMASK;
	}
	CSprite::Initialise(iSprite, iMask);
}

void CMaze::Initialise(int a_iWidth, int a_iHeight, int a_iBlockSize)
{

}

void CMaze::Process(float a_fDeltaTick)
{

}

void CMaze::Render()
{
	HDC hSpriteDC = CGame::GetInstance().GetSpriteDC();
	SelectObject(hSpriteDC, m_hSprite);
	BitBlt(CGame::GetInstance().GetBackBuffer()->GetBFDC(), 80, 100, 224, 248, hSpriteDC, 0, 0, SRCPAINT);
}

void CMaze::RenderPill(int iX, int iY)
{
	HDC hSpriteDC = CGame::GetInstance().GetSpriteDC();
	SelectObject(hSpriteDC, m_hSprite);

	float PillSize = 16;
	BitBlt(CGame::GetInstance().GetBackBuffer()->GetBFDC(), iX - PillSize / 2, iY - PillSize / 2, PillSize, PillSize, hSpriteDC, 0, 0, SRCPAINT);
}

