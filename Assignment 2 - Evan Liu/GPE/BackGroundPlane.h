#pragma once

#include <vector>

class Points2D
{
public:
	Points2D(float a_x, float a_y, float a_u) :
		x(a_x),
		y(a_y),
		i(a_u)
	{
	}

	float x; 
	float y;
	float i;
};


class CBackGroundPlane
{
public:
	CBackGroundPlane(void);
	~CBackGroundPlane(void);

	void Initialise(float a_PlaneShiftSpeed); 
	void Render();
	void Process(float a_fDeltaTick);

private:
	std::vector<Points2D>	m_pBackgroundStars; 
	float					m_fPlaneShiftSpeed;

};

