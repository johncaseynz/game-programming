#include "stdafx.h"
#include "ActorAI.h"
#include <vector>

//==============================================================================
// CActorAI
//==============================================================================
CActorAI::CActorAI(CModel *a_pModel, CActorData *a_pActorData) :
        m_pModel(a_pModel),
        m_pActorData(a_pActorData),
        m_pActorOrigin(a_pActorData->GetOrigin()),
        m_pActorPosition(a_pActorData->GetPosition()),
        m_currentMover(0),
        m_iTargetRow(-1),
        m_iTargetColumn(-1) {
    m_pLeftMover = new CActorLeftMover(m_pActorPosition, this, m_pModel);
    m_pRightMover = new CActorRightMover(m_pActorPosition, this, m_pModel);
    m_pUpMover = new CActorUpMover(m_pActorPosition, this, m_pModel);
    m_pDownMover = new CActorDownMover(m_pActorPosition, this, m_pModel);
}

CActorAI::~CActorAI() {
    delete m_pLeftMover;
    delete m_pRightMover;
    delete m_pUpMover;
    delete m_pDownMover;
}

CActorData *CActorAI::GetActorData() {
	return m_pActorData;
}

int CActorAI::GetTargetRow() {
    return m_iTargetRow;
}

void CActorAI::SetTargetRow(int a_iRow) {
    m_iTargetRow = a_iRow;
}

int CActorAI::GetTargetColumn() {
    return m_iTargetColumn;
}

void CActorAI::SetTargetColumn(int a_iColumn) {
    m_iTargetColumn = a_iColumn;
}

bool CActorAI::CanMoveToTile(int a_iRow, int a_iColumn) {
    switch (m_pModel->GetTileValue(a_iRow, a_iColumn)) {
        case TileEmpty:
        case TileDot:
        case TileEnergizer:
            return true;
        default:
            return false;
    }
}

CActorMover *CActorAI::GetMover(EDirection a_eDirection) {
    switch (a_eDirection) {
        case DirectionUp:
            return m_pUpMover;
        case DirectionDown:
            return m_pDownMover;
        case DirectionLeft:
            return m_pLeftMover;
        case DirectionRight:
            return m_pRightMover;
        default:
            return 0;
    }
}

void CActorAI::ActivateMover(CActorMover *a_pMover) {
    ActivateMover(a_pMover, 0);
}

void CActorAI::ActivateMover(CActorMover *a_pMover, float moveValue) {
    m_currentMover = a_pMover;
    m_currentMover->FindDestination();
    m_pActorData->SetDirection(m_currentMover->GetDirection());
    if (moveValue > 0) {
        m_currentMover->MoveBy(moveValue);
    }
}

void CActorAI::Process(float a_fDeltaTick) {
    // If is the first move
    if (m_iTargetRow < 0) {
		InitTargetPosition();
    }
    ProcessMove(a_fDeltaTick * PACMAN_SPEED);
}

void CActorAI::Reset() {
	m_pActorPosition->Reset(m_pActorData->GetOrigin());
	m_currentMover = 0;
	InitTargetPosition();
}

void CActorAI::InitTargetPosition() {
	m_iTargetRow = m_pModel->GetRowFromY(m_pActorPosition->GetY());
    m_iTargetColumn = m_pModel->GetColumnFromX(m_pActorPosition->GetX());
}

//==============================================================================
// CPacmanAI
//==============================================================================
CPacmanAI::CPacmanAI(CModel *a_pModel) :
        CActorAI(a_pModel, a_pModel->GetPacmanData()),
        m_eControllerDirection(DirectionNone) {
}

CPacmanAI::~CPacmanAI() {
}

EDirection const &CPacmanAI::getControllerDirection() const {
    return m_eControllerDirection;
}

void CPacmanAI::setControllerDirection(EDirection const &a_eDirection) {
    m_eControllerDirection = a_eDirection;
}

void CPacmanAI::ProcessMove(float a_fMoveValue) {
    if (m_eControllerDirection != DirectionNone) {
        if (m_currentMover == 0 || m_eControllerDirection == m_currentMover->GetOpposite()) {
            CActorMover *mover = GetMover(m_eControllerDirection);
            if (mover != 0) {
                if (mover->CanMove()) {
                    ActivateMover(mover, a_fMoveValue);
                    return;
                }
                if (mover->CanCrossScreen()) {
                    ActivateMover(mover);
                    mover->CrossScreen(a_fMoveValue);
                    return;
                }
            }
        }
    }

    // Move
    if (m_currentMover != 0) {
        m_currentMover->MoveBy(a_fMoveValue);
    }
}

void CPacmanAI::OnMoveComplete(CActorMover *mover, float remainder) {
    if (m_eControllerDirection != DirectionNone) {
        CActorMover *controllerMover = GetMover(m_eControllerDirection);
        if (controllerMover->CanMove()) {
            ActivateMover(controllerMover, remainder);
            return;
        }
    }

    if (mover->CanMove()) {
        if (remainder > 0) {
            ActivateMover(mover, remainder);
        }
        return;
    }

    if (mover->CanCrossScreen()) {
        mover->CrossScreen(remainder);
        return;
    }

    m_pActorData->SetDirection(DirectionNone);
    m_currentMover = 0;
}

//==============================================================================
// CGhostAI
//==============================================================================
CGhostAI::CGhostAI(CModel *a_pModel, CGhostData *a_pGhostData, float a_fIdleDelay) :
		CActorAI(a_pModel, a_pGhostData), m_pGhostData(a_pGhostData), 
		m_fIdleDelayTotal(a_fIdleDelay), m_fIdleDelay(a_fIdleDelay) {
}

CGhostAI::~CGhostAI() {
}

void CGhostAI::Process(float a_fDeltaTick) {
    switch (m_pGhostData->GetState()) {
        case GhostStateIdle:
            ProcessIdle(a_fDeltaTick);
            break;
        case GhostStateHunt:
            CActorAI::Process(a_fDeltaTick);
            break;
        case GhostStateBlue:
            break;
        case GhostStateDead:
            break;
        default:
            break;
    }
}

void CGhostAI::ProcessMove(float a_fMoveValue) {
    if (m_currentMover == 0) {
        FindMover(a_fMoveValue);
    } else {
        m_currentMover->MoveBy(a_fMoveValue);
    }
}

void CGhostAI::ProcessIdle(float a_fDeltaTick) {
    if (m_fIdleDelay > a_fDeltaTick) {
        m_fIdleDelay -= a_fDeltaTick;
    } else {
        m_fIdleDelay = 0;
        StartHunt(a_fDeltaTick);
    }
}

void CGhostAI::StartHunt(float a_fDeltaTick) {
    m_pActorPosition->Reset(m_pModel->GetGateway());
	InitTargetPosition();

	m_pGhostData->SetState(GhostStateHunt);
    CActorAI::Process(a_fDeltaTick);
}

void CGhostAI::OnMoveComplete(CActorMover *mover, float remainder) {
    if (mover->CanCrossScreen()) {
        mover->CrossScreen(remainder);
    } else {
        FindMover(remainder);
    }
}

void CGhostAI::Reset() {
	m_fIdleDelay = m_fIdleDelayTotal;
	
	m_pGhostData->SetState(GhostStateIdle);

	m_pActorPosition->Reset(m_pGhostData->GetOrigin());
	m_currentMover = 0;
}

void CGhostAI::FindMover(float a_fMoveValue) {
    std::vector<EDirection> directions;
    for (int i = DirectionNone + 1; i <= DirectionNone + 4; ++i) {
        EDirection direction = (EDirection) i;
        if (m_currentMover != 0 && direction == m_currentMover->GetOpposite()) {
            continue;
        }
        CActorMover *mover = GetMover(direction);
        if (mover->CanMove()) {
            directions.push_back(direction);
        }
    }
    if (directions.size() == 0 && m_currentMover != 0) {
        directions.push_back(m_currentMover->GetOpposite());
    }
    unsigned long index = (unsigned long) (rand() % directions.size());
    ActivateMover(GetMover(directions.at(index)), a_fMoveValue);
}
