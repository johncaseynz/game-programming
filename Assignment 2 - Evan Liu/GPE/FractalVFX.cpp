#include "StdAfx.h"
#include "FractalVFX.h"
#include "resource.h"
#include "Game.h"
#include "BackBuffer.h"
#include "Sprite.h"


CFractalVFX::CFractalVFX(void)
{
	m_fLimit = 2.0f;
	m_fCenterX = 0.0f;
	m_fCenterY = 0.0f;
	m_fScale = 0.02f;
	m_iLPSTEP = 5;
	m_iEndX = 300;
	m_iEndY = 300;
	m_iStartX = -300;
	m_iStartY = -300;
	m_fSTEP = 0.1f;
	m_fSCALESTEP = 0.01f; 
	m_fLimit += 106.5f * 100.0f;
}


CFractalVFX::~CFractalVFX(void)
{
}

void CFractalVFX::Render(void)
{
	for (int iX = m_iStartX; iX < m_iEndX; ++iX)
	{
		for (int iY = m_iStartY; iY < m_iEndY; ++iY)
		{
			float fax = m_fCenterX + iX * m_fScale;
			float fay = m_fCenterY + iY * m_fScale;

			float fa1 = 0.45f;
			float fb1 = 0.1428f;
			int iLP = 0;
				
			while (true)
			{
				iLP = iLP + m_iLPSTEP;

				float fa2 = fa1 * fa1 - fb1 * fb1 + fax;
				float fb2 = 2 * fa1 * fb1 + fay;

				fa1 = fa2;
				fb1 = fb2;

				if (iLP > 255)
				{
					break;
				}
				if (((fa1 * fa1) + (fb1 * fb1)) > m_fLimit)
				{
					break;
				}
			}			

			if (iLP > 255)
			{
				iLP = 0;
			}

			SetPixel(CGame::GetInstance().GetBackBuffer()->GetBFDC(), 
				iX + m_iEndX, 
				iY + m_iEndY, 
				RGB((iLP * 1.0f), 
				(iLP * 1.0f), 
				(iLP * 1.0f)));
		}	
	}
}