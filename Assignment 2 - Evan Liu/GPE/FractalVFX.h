#pragma once
class CFractalVFX
{
public:
	CFractalVFX(void);
	~CFractalVFX(void);
	void Render(void); 

private:
	float m_fLimit;
	float m_fCenterX;
	float m_fCenterY;
	float m_fScale;

	int m_iLPSTEP;

	int m_iEndX;
	int m_iEndY;
	int m_iStartX;
	int m_iStartY;
	float m_fSTEP;
	float m_fSCALESTEP;
};

