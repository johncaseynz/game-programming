#include "stdafx.h"
#include "ActorMover.h"

//==============================================================================
// CActorMover
//==============================================================================

CActorMover::CActorMover(EDirection a_eDirection, CPosition *a_pPosition, IActorMoverHandler *a_pHandler, CModel *a_pModel)  :
        m_eDirection(a_eDirection), m_pPosition(a_pPosition), m_pHandler(a_pHandler), m_pModel(a_pModel) {
    switch (a_eDirection) {
        case DirectionUp:
            m_eOpposite = DirectionDown;
            break;
        case DirectionDown:
            m_eOpposite = DirectionUp;
            break;
        case DirectionLeft:
            m_eOpposite = DirectionRight;
            break;
        case DirectionRight:
            m_eOpposite = DirectionLeft;
            break;
        default:
            m_eOpposite = DirectionNone;
            break;
    }
}

CActorMover::~CActorMover() {
}

EDirection const &CActorMover::GetDirection() const {
    return m_eDirection;
}

EDirection const &CActorMover::GetOpposite() const {
    return m_eOpposite;
}

float CActorMover::GetDestination() const {
    return m_fDestination;
}

void CActorMover::SetDestination(float a_fDestination) {
    m_fDestination = a_fDestination;
}

bool CActorMover::CanMove() {
    return CanMove(m_pHandler->GetTargetRow(), m_pHandler->GetTargetColumn());
}

bool CActorMover::CanCrossScreen() {
    return false;
}

void CActorMover::CrossScreen(float a_fMoveValue) {
}

void CActorMover::Complete(float a_fRemainder) {
    m_pHandler->OnMoveComplete(this, a_fRemainder);
}

//==============================================================================
// CActorLeftMover
//==============================================================================
void CActorLeftMover::MoveBy(float a_fValue) {
    float nextX = m_pPosition->GetX() - a_fValue;
    if (nextX <= m_fDestination) {
        m_pPosition->SetX(m_fDestination);
        Complete(m_fDestination - nextX);
    } else {
        m_pPosition->SetX(nextX);
    }
}

bool CActorLeftMover::CanMove(int a_iRow, int a_iColumn) {
    return m_pHandler->CanMoveToTile(a_iRow, a_iColumn - 1);
}

bool CActorLeftMover::CanCrossScreen() {
    if (m_pHandler->GetTargetColumn() > 0) {
        return false;
    }
    return m_pHandler->CanMoveToTile(m_pHandler->GetTargetRow(), COLUMN_COUNT - 1);
}

void CActorLeftMover::CrossScreen(float a_fMoveValue) {
    m_pPosition->SetX(m_pModel->GetCenterXOfColumn(COLUMN_COUNT - 1));
    m_pHandler->SetTargetColumn(COLUMN_COUNT - 1);
    FindDestination();

    if (a_fMoveValue > 0) {
        MoveBy(a_fMoveValue);
    }
}

void CActorLeftMover::FindDestination() {
    int row = m_pHandler->GetTargetRow();
    int column = m_pHandler->GetTargetColumn();
    while (m_pHandler->CanMoveToTile(row, column - 1)) {
        column--;
        if (m_pHandler->CanMoveToTile(row + 1, column) || m_pHandler->CanMoveToTile(row - 1, column)) {
            break;
        }
    }
    m_pHandler->SetTargetColumn(column);
    m_fDestination = m_pModel->GetCenterXOfColumn(column);
}

//==============================================================================
// CActorRightMover
//==============================================================================
void CActorRightMover::MoveBy(float a_fValue) {
    float nextX = m_pPosition->GetX() + a_fValue;
    if (nextX >= m_fDestination) {
        m_pPosition->SetX(m_fDestination);
        Complete(nextX - m_fDestination);
    } else {
        m_pPosition->SetX(nextX);
    }
}

bool CActorRightMover::CanMove(int a_iRow, int a_iColumn) {
    return m_pHandler->CanMoveToTile(a_iRow, a_iColumn + 1);
}

bool CActorRightMover::CanCrossScreen() {
    if (m_pHandler->GetTargetColumn() < COLUMN_COUNT - 1) {
        return false;
    }
    return m_pHandler->CanMoveToTile(m_pHandler->GetTargetRow(), 0);
}

void CActorRightMover::CrossScreen(float a_fMoveValue) {
    m_pPosition->SetX(m_pModel->GetCenterXOfColumn(0));
    m_pHandler->SetTargetColumn(0);
    FindDestination();

    if (a_fMoveValue > 0) {
        MoveBy(a_fMoveValue);
    }
}

void CActorRightMover::FindDestination() {
    int row = m_pHandler->GetTargetRow();
    int column = m_pHandler->GetTargetColumn();
    while (m_pHandler->CanMoveToTile(row, column + 1)) {
        column++;
        if (m_pHandler->CanMoveToTile(row + 1, column) || m_pHandler->CanMoveToTile(row - 1, column)) {
            break;
        }
    }
    m_pHandler->SetTargetColumn(column);
    m_fDestination = m_pModel->GetCenterXOfColumn(column);
}

//==============================================================================
// CActorUpMover
//==============================================================================
void CActorUpMover::MoveBy(float a_fValue) {
    float nextY = m_pPosition->GetY() - a_fValue;
    if (nextY <= m_fDestination) {
        m_pPosition->SetY(m_fDestination);
        Complete(m_fDestination - nextY);
    } else {
        m_pPosition->SetY(nextY);
    }
}

bool CActorUpMover::CanMove(int a_iRow, int a_iColumn) {
    return m_pHandler->CanMoveToTile(a_iRow - 1, a_iColumn);
}

void CActorUpMover::FindDestination() {
    int row = m_pHandler->GetTargetRow();
    int column = m_pHandler->GetTargetColumn();
    while (m_pHandler->CanMoveToTile(row - 1, column)) {
        row--;
        if (m_pHandler->CanMoveToTile(row, column + 1) || m_pHandler->CanMoveToTile(row, column - 1)) {
            break;
        }
    }
    m_pHandler->SetTargetRow(row);
    m_fDestination = m_pModel->GetCenterYOfRow(row);
}

//==============================================================================
// CActorDownMover
//==============================================================================
void CActorDownMover::MoveBy(float a_fValue) {
    float nextY = m_pPosition->GetY() + a_fValue;
    if (nextY >= m_fDestination) {
        m_pPosition->SetY(m_fDestination);
        Complete(nextY - m_fDestination);
    } else {
        m_pPosition->SetY(nextY);
    }
}

void CActorDownMover::FindDestination() {
    int row = m_pHandler->GetTargetRow();
    int column = m_pHandler->GetTargetColumn();
    while (m_pHandler->CanMoveToTile(row + 1, column)) {
        row++;
        if (m_pHandler->CanMoveToTile(row, column + 1) || m_pHandler->CanMoveToTile(row, column - 1)) {
            break;
        }
    }
    m_pHandler->SetTargetRow(row);
    m_fDestination = m_pModel->GetCenterYOfRow(row);
}

bool CActorDownMover::CanMove(int a_iRow, int a_iColumn) {
    return m_pHandler->CanMoveToTile(a_iRow + 1, a_iColumn);
}
