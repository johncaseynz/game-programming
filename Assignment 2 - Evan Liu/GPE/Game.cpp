#include "stdafx.h"
#include "Clock.h"
#include "BackBuffer.h"
#include "GPEUtils.h"
#include "Level.h"
#include "Game.h"
#include "BomberBoy.h"
#include "Controller.h"
#include "SoundFX.h"
#include "Model.h"

CGame* CGame::s_pGame = NULL;

CGame::CGame() :
		m_hApplicationInstance(NULL),
		m_hMainWindow(NULL),
		m_pClock(NULL),
		m_pBackBuffer(NULL),
		m_pController(NULL),
		m_pLevel(NULL), 
		m_pModel(NULL),
		isStart(true)
{
}

CGame::~CGame()
{
	delete m_pBackBuffer;
	m_pBackBuffer = NULL;
	delete m_pLevel; 
	m_pLevel = NULL;
	delete m_pClock;
	m_pClock = NULL;
	delete m_pController; 
	m_pController = NULL; 
	delete m_pSoundFX; 
	m_pSoundFX = NULL; 
}

bool CGame::Initialise(HINSTANCE a_hInstance, HWND a_hWnd, int a_iWidth, int a_iHeight)
{
	m_pModel = new CModel();
	m_pModel->SetMapPosition(80, 100);
	m_pModel->ReadMap(DEFAULT_MAP);

	m_hApplicationInstance = a_hInstance;
	assert(m_hApplicationInstance);
	m_hMainWindow = a_hWnd;
	assert(m_hMainWindow); 
	m_hSpriteDC = CreateCompatibleDC(NULL);
	assert(m_hSpriteDC); 
	m_pBackBuffer = new CBackBuffer();
	assert(m_pBackBuffer);
	m_pBackBuffer->Initialise(a_hWnd, a_iWidth, a_iHeight);
	m_pLevel = new CLevel();
	assert(m_pLevel);
	m_pController = new CController(); 
	assert(m_pController);
	m_pClock = new CClock; 
	assert(m_pClock); 
	m_pSoundFX = new CSoundFX(a_hInstance); 
	assert(m_pSoundFX); 
	if (!m_pLevel->Initialise(a_iWidth, a_iHeight))
	{
		// Initialise level specific information here.
		// This could be points per level, random encounters for the level
	}
	ShowCursor(false);
	ProcessKeyDownMessage(VK_LEFT);
	return (true);
}

void CGame::Draw()
{
	m_pBackBuffer->Clear();
	m_pLevel->Render();
	m_pBackBuffer->Present();
	if (isStart){
		m_pSoundFX->Play();
		isStart = false;
	}
}

void CGame::Process(float a_fDeltaTick)
{
	m_pModel->Process(a_fDeltaTick);
	m_pLevel->Process(a_fDeltaTick);
	m_pSoundFX->Process(a_fDeltaTick); 
}

void CGame::ExecuteOneFrame()
{
	if (!m_pModel->GetWin() && !m_pModel->GetLose())
	{
		float fDT = m_pClock->GetDeltaTick();
		Process(fDT);
		Draw();
		m_pClock->Process();
		Sleep(1);
	}

}

CGame& CGame::GetInstance()
{
	if (NULL == s_pGame)
	{
		assert(NULL == s_pGame);
		s_pGame = new CGame();
		assert(s_pGame); 
	}
	return (*s_pGame);
}

void CGame::DestroyInstance()
{
	assert(s_pGame != NULL);
	if(s_pGame != NULL)
	{
		assert(s_pGame != NULL);
		delete s_pGame;
		s_pGame = NULL;
	}
}

CBackBuffer* CGame::GetBackBuffer()
{
	return (m_pBackBuffer);
}

HINSTANCE CGame::GetAppInstance()
{
	return (m_hApplicationInstance);
}

HWND CGame::GetWindow()
{
	return (m_hMainWindow);
}

HDC CGame::GetSpriteDC()
{
	return (m_hSpriteDC);
}

CLevel* CGame::GetLevel()
{
	return (m_pLevel);
}

CModel* CGame::GetModel()
{
	return (m_pModel);
}

void CGame::ProcessKeyDownMessage(char a_wParam)
{
	m_pController->ProcessKeyDownMessage(a_wParam); 
}

void CGame::ProcessKeyUpMessage(char a_wParam)
{
	m_pController->ProcessKeyUpMessage(a_wParam); 
}

CSoundFX* CGame::GetSoundFXPlayer()
{
	return m_pSoundFX;
}

void CGame::RestartGame()
{
	m_pModel->Reset(DEFAULT_MAP);
	m_pClock->Reset();
}