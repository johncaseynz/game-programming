#include "StdAfx.h"
#include "Ghost.h"
#include "Resource.h"
#include "AnimatedSprite.h"
#include "Model.h"

const float CONST_WALK_SPEED = 100.0f;


CGhost::CGhost(void) :
m_pData(0),
m_eFacing(FACING_FORWARD),
m_eDirection(FACING_FORWARD)
{
}


CGhost::~CGhost(void)
{
	Deinitialise();
}

bool CGhost::Deinitialise()
{
	return (true);
}

bool CGhost::Initialise(CActorData *a_pData, EGhostColour ghostColour)
{
	m_pData = a_pData;
	m_pPosition = m_pData->GetPosition();
	for (int i = INVALID_FACING + 1; i < MAX_FACING; i++)
	{
		m_bWalking[i] = false;
	}

	int iMask = 0;
	int iSprite = 0;

	iMask = IDB_GHOSTMASK;

	if (ghostColour == GHOSTCOLOURRED)
	{
		iSprite = IDB_GHOSTRED;
	}
	else if (ghostColour == GHOSTCOLOURPINK)
	{
		iSprite = IDB_GHOSTPINK;
	}
	else if (ghostColour == GHOSTCOLOURCYAN)
	{
		iSprite = IDB_GHOSTCYAN;
	}
	else if (ghostColour == GHOSTCOLOURORANGE)
	{
		iSprite = IDB_GHOSTORANGE;
	}

	m_pAnim[FACING_FORWARD] = new CAnimatedSprite();
	assert(m_pAnim[FACING_FORWARD]);
	m_pAnim[FACING_FORWARD]->Initialise(iSprite, iMask);
	m_pAnim[FACING_FORWARD]->SetWidth(16);
	m_pAnim[FACING_FORWARD]->SetSpeed(0.0f);
	m_pAnim[FACING_FORWARD]->AddFrame(0);
	m_pAnim[FACING_FORWARD]->AddFrame(16);
	m_pAnim[FACING_FORWARD]->AddFrame(32);
	m_pAnim[FACING_FORWARD]->AddFrame(16);

	m_pAnim[FACING_BACKWARD] = new CAnimatedSprite();
	assert(m_pAnim[FACING_BACKWARD]);
	m_pAnim[FACING_BACKWARD]->Initialise(iSprite, iMask);
	m_pAnim[FACING_BACKWARD]->SetWidth(16);
	m_pAnim[FACING_BACKWARD]->SetSpeed(0.0f);
	m_pAnim[FACING_BACKWARD]->AddFrame(144);
	m_pAnim[FACING_BACKWARD]->AddFrame(160);
	m_pAnim[FACING_BACKWARD]->AddFrame(176);

	m_pAnim[FACING_LEFT] = new CAnimatedSprite();
	assert(m_pAnim[FACING_LEFT]);
	m_pAnim[FACING_LEFT]->Initialise(iSprite, iMask);
	m_pAnim[FACING_LEFT]->SetWidth(16);
	m_pAnim[FACING_LEFT]->SetSpeed(0.0f);
	m_pAnim[FACING_LEFT]->AddFrame(96);
	m_pAnim[FACING_LEFT]->AddFrame(112);
	m_pAnim[FACING_LEFT]->AddFrame(128);

	m_pAnim[FACING_RIGHT] = new CAnimatedSprite();
	assert(m_pAnim[FACING_RIGHT]);
	m_pAnim[FACING_RIGHT]->Initialise(iSprite, iMask);
	m_pAnim[FACING_RIGHT]->SetWidth(16);
	m_pAnim[FACING_RIGHT]->SetSpeed(0.0f);
	m_pAnim[FACING_RIGHT]->AddFrame(48);
	m_pAnim[FACING_RIGHT]->AddFrame(64);
	m_pAnim[FACING_RIGHT]->AddFrame(80);

	m_eFacing = FACING_FORWARD;

	return (true);
}

void CGhost::Render()
{
	m_pAnim[m_eFacing]->Render();
}

void CGhost::checkFacing()
{
	if (m_pData != 0)
	{
		switch (m_pData->GetDirection())
		{
		case DirectionNone:
			setStopWalkingState();
			break;
		case DirectionLeft:
			setWalkState(FACING_LEFT);
			break;
		case DirectionRight:
			setWalkState(FACING_RIGHT);
			break;
		case DirectionDown:
			setWalkState(FACING_FORWARD);
			break;
		case DirectionUp:
			setWalkState(FACING_BACKWARD);
			break;
		}
	}
}

void CGhost::Process(float a_fDeltaTick)
{
	checkFacing();
	m_pAnim[m_eFacing]->Process(a_fDeltaTick);
	m_pAnim[m_eFacing]->SetX(static_cast<int>(m_pPosition->GetX()));
	m_pAnim[m_eFacing]->SetY(static_cast<int>(m_pPosition->GetY()));
}

CBoundingRect CGhost::GetBoundingRectForNextFrame(float a_fDeltaTick)
{
	if (m_bWalking[m_eFacing])
	{
		switch (m_eFacing)
		{
			case FACING_FORWARD:
			{
				m_BoundingRect.y1 = (m_pPosition->GetY() - 16) + CONST_WALK_SPEED * a_fDeltaTick;
				break;
			}
			case FACING_BACKWARD:
			{
				m_BoundingRect.y1 = (m_pPosition->GetY() - 16) - CONST_WALK_SPEED * a_fDeltaTick;
				break;
			}
			case FACING_LEFT:
			{
				m_BoundingRect.x1 = (m_pPosition->GetX() - 16) - CONST_WALK_SPEED * a_fDeltaTick;
				break;
			}
			case FACING_RIGHT:
			{
				m_BoundingRect.x1 = (m_pPosition->GetX() - 16) + CONST_WALK_SPEED * a_fDeltaTick;
				break;
			}
		}
	}

	m_BoundingRect.x2 = m_BoundingRect.x1 + 32;
	m_BoundingRect.y2 = m_BoundingRect.y1 + 32;

	return m_BoundingRect;
}

void CGhost::walk()
{
	m_pAnim[m_eFacing]->SetSpeed(0.1f);
	m_bWalking[m_eFacing] = true;
}

void CGhost::setWalkState(EFacing a_eDirection)
{
	m_eFacing = a_eDirection;
	walk();
}

void CGhost::setStopWalkingState()
{
	m_pAnim[m_eDirection]->SetSpeed(0.0f);
	m_bWalking[m_eFacing] = false;
}

void CGhost::warpRight()
{
	m_pPosition->SetX(288);
	m_pPosition->SetY(216);
}

void CGhost::warpLeft()
{
	m_pPosition->SetX(96);
	m_pPosition->SetY(216);
}