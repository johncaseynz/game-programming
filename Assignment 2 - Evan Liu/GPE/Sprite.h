#pragma once

#include "windows.h"

class CSprite
{
	public:
		CSprite();
		virtual ~CSprite();

		virtual bool Deinitialise();
		virtual bool Initialise(int a_iResourceID, int a_iMaskResourceID);

		virtual void Render();
		virtual void Process(float a_fDeltaTick);

		int GetWidth() const;
		int GetHeight() const;

		void SetX(int a_iX);
		void SetY(int a_iY);

	protected:
		int m_iX;
		int m_iY;
	
		HBITMAP m_hSprite;
		HBITMAP m_hMask;

		BITMAP m_BitmapSprite;
		BITMAP m_BitmapMask;
};


