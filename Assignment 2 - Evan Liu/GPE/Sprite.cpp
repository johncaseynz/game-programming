#include "stdafx.h"
#include "resource.h"
#include "Game.h"
#include "BackBuffer.h"
#include "Sprite.h"

CSprite::CSprite() : 
	m_iX(0), 
	m_iY(0)
{

}

CSprite::~CSprite()
{
	Deinitialise();
}

bool CSprite::Deinitialise()
{
	DeleteObject(m_hSprite);
	DeleteObject(m_hMask);
	return (true);
}

bool CSprite::Initialise(int a_iSpriteResourceID, int a_iMaskResourceID)
{
	HINSTANCE hInstance = CGame::GetInstance().GetAppInstance();

	m_hSprite = LoadBitmap(hInstance, MAKEINTRESOURCE(a_iSpriteResourceID));
	m_hMask = LoadBitmap(hInstance, MAKEINTRESOURCE(a_iMaskResourceID));

	GetObject(m_hSprite, sizeof(BITMAP), &m_BitmapSprite);
	GetObject(m_hMask, sizeof(BITMAP), &m_BitmapMask);
	return (true);
}



void CSprite::Render()
{
	int iW = GetWidth();
	int iH = GetHeight();
	int iX = m_iX - (iW / 2);
	int iY = m_iY - (iH / 2);

	HDC hSpriteDC = CGame::GetInstance().GetSpriteDC();
	HGDIOBJ hOldObj = SelectObject(hSpriteDC, m_hMask);
	BitBlt(CGame::GetInstance().GetBackBuffer()->GetBFDC(), iX, iY, iW, iH, hSpriteDC, 0, 0, SRCAND);
	SelectObject(hSpriteDC, m_hSprite);
	BitBlt(CGame::GetInstance().GetBackBuffer()->GetBFDC(), iX, iY, iW, iH, hSpriteDC, 0, 0, SRCPAINT);
	SelectObject(hSpriteDC, hOldObj);
}

void CSprite::Process(float a_fDeltaTick)
{
}

int CSprite::GetWidth() const
{
	return (m_BitmapSprite.bmWidth);
}

int CSprite::GetHeight() const
{
	return (m_BitmapSprite.bmHeight);
}

void CSprite::SetX(int a_iX)
{
	m_iX = a_iX;
}

void CSprite::SetY(int a_iY)
{
	m_iY = a_iY;
}
