#include "StdAfx.h"
#include "Game.h"
#include "BackBuffer.h"
#include "AnimatedSprite.h"
#include "GPEUtils.h"
#include "BackGroundPlane.h"


CBackGroundPlane::CBackGroundPlane(void)
{
}


CBackGroundPlane::~CBackGroundPlane(void)
{
}

void CBackGroundPlane::Initialise(float a_PlaneShiftSpeed)
{
	m_fPlaneShiftSpeed = a_PlaneShiftSpeed;
	for(size_t x = 0; x < 10; x++)
	{
		for(size_t y = 0; y < 10; y++)
		{
			m_pBackgroundStars.push_back(Points2D(float(x)*10, float(y)*10, 1)); 
		}
	}
}

void CBackGroundPlane::Process(float a_fDeltaTick)
{
	for(size_t i = 0; i < m_pBackgroundStars.size(); ++i)
	{
		m_pBackgroundStars[i].x += a_fDeltaTick * m_fPlaneShiftSpeed;
	}
}

void CBackGroundPlane::Render()
{
	COLORREF old_fCol, old_bCol;
	int old_bMode;

	old_fCol = SetTextColor(CGame::GetInstance().GetBackBuffer()->GetBFDC(), RGB(255, 0, 0));
	old_bCol = SetBkColor(CGame::GetInstance().GetBackBuffer()->GetBFDC(), RGB(0, 0, 0));
	old_bMode = SetBkMode(CGame::GetInstance().GetBackBuffer()->GetBFDC(), OPAQUE);
	
	for(size_t i = 0; i < m_pBackgroundStars.size(); ++i)
	{
		int iX = static_cast<int>(m_pBackgroundStars.at(i).x);
		int iY = static_cast<int>(m_pBackgroundStars.at(i).y);

		SetPixel(CGame::GetInstance().GetBackBuffer()->GetBFDC(), iX, iY, RGB(255, 0, 0));
	}

	SetPixel(CGame::GetInstance().GetBackBuffer()->GetBFDC(), 100, 100, RGB(255, 255, 0));
	SetPixel(CGame::GetInstance().GetBackBuffer()->GetBFDC(), 100, 101, RGB(255, 255, 0));
	SetPixel(CGame::GetInstance().GetBackBuffer()->GetBFDC(), 100, 102, RGB(255, 255, 0));
	SetPixel(CGame::GetInstance().GetBackBuffer()->GetBFDC(), 101, 100, RGB(255, 255, 0));
	SetPixel(CGame::GetInstance().GetBackBuffer()->GetBFDC(), 101, 101, RGB(255, 255, 0));
	SetPixel(CGame::GetInstance().GetBackBuffer()->GetBFDC(), 101, 102, RGB(255, 255, 0));

	SetTextColor(CGame::GetInstance().GetBackBuffer()->GetBFDC(), old_fCol);
	SetBkColor(CGame::GetInstance().GetBackBuffer()->GetBFDC(), old_bCol);
	SetBkMode(CGame::GetInstance().GetBackBuffer()->GetBFDC(), old_bMode);
}