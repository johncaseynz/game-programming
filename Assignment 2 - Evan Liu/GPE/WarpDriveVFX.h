#pragma once

#include "BoundingRect.h"

class CWarpDriveVFX
{
public:
	CWarpDriveVFX(void);
	~CWarpDriveVFX(void);

	void Initialise(float a_fWarpSpeed); 
	void Render();
	void Process(float a_fDeltaTick);
	CBoundingRect GetBoundingRectForNextFrame(float a_fDeltaTick);
	CBoundingRect GetBoundingRect2ForNextFrame(float a_fDeltaTick);

private:
	float	m_fWarpSpeed; 
	float	m_AnimationAngle;

};

