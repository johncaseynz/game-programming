#pragma once

#include "resource.h"
#include <strstream>

template<typename T> std::string ToString(const T& pvalue)
{
	std::strstream theStream;
	theStream << pvalue << std::ends;
	return (theStream.str());
}
