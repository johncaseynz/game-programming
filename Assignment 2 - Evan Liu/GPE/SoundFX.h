#pragma once

#include <windef.h>

class CSoundFX
{
public:

		enum ESoundFXPlay
		{
			INVALIDSOUND = -1,
			NOSOUND,
			BEAMSOUND,
			ENERGIZESOUND,
			WARPSOUND,
			TELEPORTSOUND,
			PACMANBEGIN,
			PACMANCHOMP,
			PACMANEATGHOST
		};

		CSoundFX(HINSTANCE a_hInstance);
		~CSoundFX(void);

		void Play(); 
		void Process(float a_fDeltaTick);
		void SetSoundFXState(ESoundFXPlay a_eSoundFXPlay);

	private:
		void PlayTeleportSound();
		void PlayWarpSound();
		void PlayEnergizeSound(); 
		void PlayBeamSound();
		void PlayBeginGame();
		void PlayPacmanChomp();
		void PlayPacmanEatGhost();

	private:
		ESoundFXPlay	m_eSoundFXPlay;
		bool			m_bPlaying;
		HINSTANCE		m_hInstance;
		float			m_fSoundRepeat;
		float			m_fTimeElapsed;

};

