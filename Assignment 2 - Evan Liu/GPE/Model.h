#pragma once

class CPacmanAI;
class CGhostAI;

//==============================================================================
// Constants
//==============================================================================
#define TILE_SIZE           8
#define ROW_COUNT           31
#define COLUMN_COUNT        28

#define PACMAN_SIZE         16
#define GHOST_SIZE          16
#define DOT_SIZE            2
#define ENERGIZER_SIZE      6

#define PACMAN_SPEED        48
#define GHOST_SPEED         48
#define GHOST_BLUE_SPEED    40

enum EGameState {
    GameStateNone,
    GameStateRunning,
    GameStateWin,
    GameStateLose,
};

enum EDirection {
    DirectionNone,
    DirectionUp,
    DirectionDown,
    DirectionLeft,
    DirectionRight,
};

enum ETileValue {
    TileOut = -1,
    TileEmpty = 0,
    TileWall = 1,
    TileGate = 2,
    TileDot = 3,
    TileEnergizer = 4,
    TilePacman = 5,
    TileRed = 6,
    TileCyan = 7,
    TilePink = 8,
    TileOrange = 9
};

enum EGhostState {
    GhostStateIdle,
    GhostStateHunt,
    GhostStateBlue,
    GhostStateDead,
};

static int DEFAULT_MAP[ROW_COUNT][COLUMN_COUNT] = {
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
        {1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1},
        {1, 3, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 3, 1},
        {1, 4, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 4, 1},
        {1, 3, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 3, 1},
        {1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1},
        {1, 3, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 3, 1},
        {1, 3, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 3, 1},
        {1, 3, 3, 3, 3, 3, 3, 1, 1, 3, 3, 3, 3, 1, 1, 3, 3, 3, 3, 1, 1, 3, 3, 3, 3, 3, 3, 1},
        {1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 3, 1, 1, 0, 0, 0, 0, 6, 6, 0, 0, 0, 0, 1, 1, 3, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 3, 1, 1, 0, 1, 1, 1, 2, 2, 1, 1, 1, 0, 1, 1, 3, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 3, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 3, 1, 1, 1, 1, 1, 1},
        {0, 0, 0, 0, 0, 0, 3, 0, 0, 0, 1, 7, 7, 8, 8, 9, 9, 1, 0, 0, 0, 3, 0, 0, 0, 0, 0, 0},
        {1, 1, 1, 1, 1, 1, 3, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 3, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 3, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 3, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 3, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 3, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 3, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 3, 1, 1, 1, 1, 1, 1},
        {1, 1, 1, 1, 1, 1, 3, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 3, 1, 1, 1, 1, 1, 1},
        {1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1, 1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1},
        {1, 3, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 3, 1},
        {1, 3, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 3, 1},
        {1, 4, 3, 3, 1, 1, 3, 3, 3, 3, 3, 3, 3, 5, 5, 3, 3, 3, 3, 3, 3, 3, 1, 1, 3, 3, 4, 1},
        {1, 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 1},
        {1, 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 1},
        {1, 3, 3, 3, 3, 3, 3, 1, 1, 3, 3, 3, 3, 1, 1, 3, 3, 3, 3, 1, 1, 3, 3, 3, 3, 3, 3, 1},
        {1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1},
        {1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1},
        {1, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 1},
        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1}
};

//==============================================================================
// Position
//==============================================================================
class CPosition {

public:

    CPosition() : m_fX(0), m_fY(0) {
    }

    CPosition(float a_fX, float a_fY) : m_fX(a_fX), m_fY(a_fY) {
    }

    virtual ~CPosition() {
    }


    float GetX() const {
        return m_fX;
    }

    void SetX(float a_fX) {
        m_fX = a_fX;
    }

    float GetY() const {
        return m_fY;
    }

    void SetY(float a_fY) {
        m_fY = a_fY;
    }

    void Reset(float p_fX, float p_fY) {
        m_fX = p_fX;
        m_fY = p_fY;
    };

    void Reset(CPosition *a_pPosition) {
        m_fX = a_pPosition->GetX();
        m_fY = a_pPosition->GetY();
    };

private:
    float m_fX;
    float m_fY;
};

//==============================================================================
// ActorData
//==============================================================================

class CActorData {

public:
    CActorData() {
        m_eDirection = DirectionNone;
        m_pOrigin = new CPosition();
        m_pPosition = new CPosition();
    }

    virtual ~CActorData() {
        delete m_pOrigin;
        delete m_pPosition;
    }

    CPosition *GetOrigin() const {
        return m_pOrigin;
    }

    CPosition *GetPosition() const {
        return m_pPosition;
    }

    EDirection const &GetDirection() const {
        return m_eDirection;
    }

    void SetDirection(EDirection const &a_eDirection) {
        m_eDirection = a_eDirection;
    }

private:
    CPosition *m_pOrigin;
    CPosition *m_pPosition;
    EDirection m_eDirection;
};

//==============================================================================
// GhostData
//==============================================================================

class CGhostData : public CActorData {

public:

    CGhostData() : m_eState(GhostStateIdle) {
    }

    virtual ~CGhostData() {
    }

    EGhostState const &GetState() {
        return m_eState;
    }

    void SetState(EGhostState const &a_eState) {
        m_eState = a_eState;
    }

private:
    EGhostState m_eState;

};

class ITileMap {

public:
    virtual int GetTileSize() = 0;

    virtual int GetRows() = 0;

    virtual int GetColumns() = 0;

    virtual int GetWidth() = 0;

    virtual int GetHeight() = 0;

    virtual float GetXOfColumn(int a_iColumn) = 0;

    virtual float GetYOfRow(int a_iRow) = 0;

    virtual float GetCenterXOfColumn(int a_iColumn) = 0;

    virtual float GetCenterYOfRow(int a_iRow) = 0;

    virtual int GetColumnFromX(float a_fX) = 0;

    virtual int GetRowFromY(float a_fY) = 0;

    virtual ETileValue GetTileValue(int a_iRow, int a_iColumn) = 0;

};

//==============================================================================
// Model
//==============================================================================
class CModel : public ITileMap {

public:
    CModel();

    virtual ~CModel();

    //-- ITileMap --//

    virtual int GetTileSize();

    virtual int GetRows();

    virtual int GetColumns();

    virtual int GetWidth();

    virtual int GetHeight();

    virtual float GetXOfColumn(int a_iColumn);

    virtual float GetYOfRow(int a_iRow);

    virtual float GetCenterXOfColumn(int a_iColumn);

    virtual float GetCenterYOfRow(int a_iRow);

    virtual int GetColumnFromX(float a_fX);

    virtual int GetRowFromY(float a_fY);

    virtual ETileValue GetTileValue(int a_iRow, int a_iColumn);

    //-- Status --//

    EGameState GetGameState();

    bool GetRunning();

    bool GetGameOver();

    bool GetWin();

    bool GetLose();

    int GetScore();
	
	int GetPacmanLife();

    //-- Actors --//

    CActorData *GetPacmanData();

    CGhostData *GetRedData();

    CGhostData *GetCyanData();

    CGhostData *GetPinkData();

    CGhostData *GetOrangeData();

    //-- Map --//

    CPosition *GetMapPosition();

    void SetMapPosition(float a_fX, float a_fY);

    void ReadMap(int a_iMap[ROW_COUNT][COLUMN_COUNT]);

    CPosition *GetGateway();

    //-- Controller --//
    EDirection GetControllerDirection();
    void SetControllerDirection(EDirection const &a_eControllerDirection);

	//-- Process --//
	void Process(float a_fDeltaTick);
	void Reset(int a_iMap[ROW_COUNT][COLUMN_COUNT]);

private:
    int m_iTileSize;
    int m_iRows;
    int m_iColumns;
    int m_iWidth;
    int m_iHeight;

	int m_iDotCount;
	int m_iPacmanLife;

    EGameState m_eGameState;
    int m_iScore;

    ETileValue m_map[ROW_COUNT][COLUMN_COUNT];

    CPosition *m_pMapPosition;
    CPosition *m_pGateway;

    CActorData *m_pPacmanData;

    CGhostData *m_pRedData;
    CGhostData *m_pCyanData;
    CGhostData *m_pPinkData;
    CGhostData *m_pOrangeData;
	CGhostAI *m_aGhostAIs[4];

    void ReadActorPositionFromMap(CActorData *a_pActorData, int &a_iRow, int &a_iColumn);

	void TouchedGhost(CGhostAI *a_pGhostAI);

	void AteDot();

	void AteEnergizer();

public: // Public only for unit test
    CPacmanAI *m_pPacmanAI;

};
