#include "stdafx.h"
#include "Model.h"
#include <map>
#include "ActorAI.h"
#include "Game.h"
#include "SoundFX.h"

//==============================================================================
// Model
//==============================================================================
CModel::CModel() :
		m_iPacmanLife(3),
        m_iScore(0),
        m_iTileSize(TILE_SIZE),
        m_iRows(ROW_COUNT),
        m_iColumns(COLUMN_COUNT),
        m_eGameState(GameStateNone) {
    m_iWidth = m_iTileSize * m_iRows;
    m_iHeight = m_iTileSize * m_iColumns;

    m_pMapPosition = new CPosition();
    m_pGateway = new CPosition();

    m_pPacmanData = new CActorData();
    m_pRedData = new CGhostData();
    m_pCyanData = new CGhostData();
    m_pPinkData = new CGhostData();
    m_pOrangeData = new CGhostData();

    m_pPacmanAI = new CPacmanAI(this);
    m_aGhostAIs[0] = new CGhostAI(this, m_pRedData, 1);
    m_aGhostAIs[1] = new CGhostAI(this, m_pCyanData, 5);
    m_aGhostAIs[2] = new CGhostAI(this, m_pPinkData, 3);
    m_aGhostAIs[3] = new CGhostAI(this, m_pOrangeData, 7);
}

CModel::~CModel() {
    delete m_pMapPosition;
    delete m_pGateway;

    delete m_pPacmanData;
    delete m_pRedData;
    delete m_pCyanData;
    delete m_pPinkData;
    delete m_pOrangeData;
}

int CModel::GetTileSize() {
    return m_iTileSize;
}

int CModel::GetRows() {
    return m_iRows;
}

int CModel::GetColumns() {
    return m_iColumns;
}

int CModel::GetWidth() {
    return m_iWidth;
}

int CModel::GetHeight() {
    return m_iHeight;
}

float CModel::GetXOfColumn(int a_iColumn) {
    return m_pMapPosition->GetX() + m_iTileSize * a_iColumn;
}

float CModel::GetYOfRow(int a_iRow) {
    return m_pMapPosition->GetY() + m_iTileSize * a_iRow;
}

float CModel::GetCenterXOfColumn(int a_iColumn) {
    return GetXOfColumn(a_iColumn) + m_iTileSize / 2;
}

float CModel::GetCenterYOfRow(int a_iRow) {
    return GetYOfRow(a_iRow) + m_iTileSize / 2;
}

int CModel::GetColumnFromX(float a_fX) {
    return (int) ((a_fX - m_pMapPosition->GetX()) / m_iTileSize);
}

int CModel::GetRowFromY(float a_fY) {
    return (int) ((a_fY - m_pMapPosition->GetY()) / m_iTileSize);
}

EGameState CModel::GetGameState() {
    return m_eGameState;
}

bool CModel::GetRunning() {
    return m_eGameState == GameStateRunning;
}

bool CModel::GetGameOver() {
    return m_eGameState == GameStateWin || m_eGameState == GameStateLose;
}

bool CModel::GetWin() {
    return m_eGameState == GameStateWin;
}

bool CModel::GetLose() {
    return m_eGameState == GameStateLose;
}

int CModel::GetScore() {
    return m_iScore;
}

int CModel::GetPacmanLife()
{
	return m_iPacmanLife;
}

ETileValue CModel::GetTileValue(int a_iRow, int a_iColumn) {
    if (a_iRow < 0 || a_iRow >= ROW_COUNT || a_iColumn < 0 || a_iColumn >= COLUMN_COUNT) {
        return TileOut;
    }
    return m_map[a_iRow][a_iColumn];
}

CActorData *CModel::GetPacmanData() {
    return m_pPacmanData;
}

CGhostData * CModel::GetRedData() {
    return m_pRedData;
}

CGhostData * CModel::GetCyanData() {
    return m_pCyanData;
}

CGhostData * CModel::GetPinkData() {
    return m_pPinkData;
}

CGhostData * CModel::GetOrangeData() {
    return m_pOrangeData;
}

CPosition *CModel::GetMapPosition() {
    return m_pMapPosition;
}

void CModel::SetMapPosition(float a_fX, float a_fY) {
    m_pMapPosition->SetX(a_fX);
    m_pMapPosition->SetY(a_fY);
}

void CModel::ReadMap(int a_iMap[ROW_COUNT][COLUMN_COUNT]) {
    std::map<int, int> actorRows;
    std::map<int, int> actorColumns;
    int gateRow = -1;
    int gateColumn = -1;
	m_iDotCount = 0;
    for (int row = 0; row < ROW_COUNT; ++row) {
        for (int column = 0; column < COLUMN_COUNT; ++column) {
            int value = a_iMap[row][column];
            if (value >= TilePacman) {
                actorRows[value] = row;
                actorColumns[value] = column;
                value = TileEmpty;
            } else if (value == TileGate) {
                gateRow = row;
                gateColumn = column;
            } else if (value == TileDot || value == TileEnergizer) {
				m_iDotCount++;
			}
            m_map[row][column] = (ETileValue) value;
        }
    }

    ReadActorPositionFromMap(m_pPacmanData, actorRows[TilePacman], actorColumns[TilePacman]);
    ReadActorPositionFromMap(m_pRedData, actorRows[TileRed], actorColumns[TileRed]);
    ReadActorPositionFromMap(m_pCyanData, actorRows[TileCyan], actorColumns[TileCyan]);
    ReadActorPositionFromMap(m_pPinkData, actorRows[TilePink], actorColumns[TilePink]);
    ReadActorPositionFromMap(m_pOrangeData, actorRows[TileOrange], actorColumns[TileOrange]);

    m_pGateway->Reset(GetXOfColumn(gateColumn), GetCenterYOfRow(gateRow - 1));
}

void CModel::ReadActorPositionFromMap(CActorData *a_pActorData, int &a_iRow, int &a_iColumn) {
    float x = GetXOfColumn(a_iColumn);
    float y = GetCenterYOfRow(a_iRow);
    a_pActorData->GetOrigin()->Reset(x, y);
    a_pActorData->GetPosition()->Reset(x, y);
}

CPosition *CModel::GetGateway() {
    return m_pGateway;
}

EDirection CModel::GetControllerDirection() {
    return m_pPacmanAI->getControllerDirection();
}

void CModel::SetControllerDirection(EDirection const &a_eControllerDirection) {
    m_pPacmanAI->setControllerDirection(a_eControllerDirection);
}

void CModel::Process(float a_fDeltaTick) {
    m_pPacmanAI->Process(a_fDeltaTick);

	CPosition *pacmanPosition = m_pPacmanData->GetPosition();
	int pacmanRow = GetRowFromY(pacmanPosition->GetY());
	int pacmanColumn = GetColumnFromX(pacmanPosition->GetX());

	for (int i = 0; i < 4; ++i) {
		CGhostAI *ghostAI = m_aGhostAIs[i];
		ghostAI->Process(a_fDeltaTick);
		CPosition *ghostPosition = ghostAI->GetActorData()->GetPosition();
		int ghostRow = GetRowFromY(ghostPosition->GetY());
		int ghostColumn = GetColumnFromX(ghostPosition->GetX());
		if (ghostRow == pacmanRow) {
			if (abs(ghostPosition->GetX() - pacmanPosition->GetX()) < PACMAN_SIZE) {
				TouchedGhost(ghostAI);
			}
		} else if (ghostColumn == pacmanColumn) {
			if (abs(ghostPosition->GetY() - pacmanPosition->GetY()) < PACMAN_SIZE) {
				TouchedGhost(ghostAI);
			}
		}
	}
	
	int tiles[5][2] = {
		{pacmanRow, pacmanColumn - 1}, 
		{pacmanRow, pacmanColumn},
		{pacmanRow, pacmanColumn + 1},
		{pacmanRow - 1, pacmanColumn},
		{pacmanRow + 1, pacmanColumn},
	};

    for (int i = 0; i < 5; ++i) {
		int row = tiles[i][0];
		int column = tiles[i][1];
        ETileValue tileValue = GetTileValue(row, column);
        switch (tileValue) {
            case TileDot:
            case TileEnergizer:
				float distance;
                if (row == pacmanRow) {
					distance = abs(GetCenterXOfColumn(column) - pacmanPosition->GetX());
				} else {
					distance = abs(GetCenterYOfRow(row) - pacmanPosition->GetY());
				}
				if (distance <= DOT_SIZE) {
					CGame::GetInstance().GetSoundFXPlayer()->SetSoundFXState(CSoundFX::PACMANCHOMP);
					if (tileValue == TileDot) {
						AteDot();
					} else {
						AteEnergizer();
					}
					m_map[row][column] = TileEmpty;
					m_iScore++;
					m_iDotCount--;
					if (m_iDotCount == 0) {
						m_eGameState = GameStateWin;
					}
				}
                break;
            default:
                break;
        }
    }
}

void CModel::TouchedGhost(CGhostAI *a_pGhostAI) {
	// TODO Pacman lose life
	m_iPacmanLife--;
	if (m_iPacmanLife == -1)
	{
		m_eGameState = GameStateLose;
	}
	m_pPacmanAI->Reset();
}

void CModel::AteDot() {
	CGame::GetInstance().GetSoundFXPlayer()->Play();
}

void CModel::AteEnergizer() {
	for (int i = 0; i < 4; ++i) {
		m_aGhostAIs[i]->Reset();
	}
}

void CModel::Reset(int a_iMap[ROW_COUNT][COLUMN_COUNT])
{
	m_iPacmanLife = 3;
    m_iScore = 0;
    m_eGameState = GameStateNone;

	for (int i = 0; i < 4; ++i) {
		m_aGhostAIs[i]->Reset();
	}
	m_pPacmanAI->Reset();
	ReadMap(a_iMap);
}