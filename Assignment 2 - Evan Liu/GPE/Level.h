#pragma once

#include <WinDef.h>

class CBomberBoy;
class CSoundFX;
class CBackGroundPlane; 
class CWarpDriveVFX; 
class CFractalVFX;
class CMaze;
class CActorData;
class CModel;
class CPosition;
class CPacman;
class CGhost;

class CLevel
{
	public:
		CLevel();
		virtual ~CLevel();

		virtual bool Deinitialise();
		virtual bool Initialise(int a_iWidth, int a_iHeight);

		virtual void Render();

		virtual void Process(float a_fDeltaTick);
		void		 Play();

		CBomberBoy* GetBomberBoyBlack();
		CBomberBoy* GetBomberBoyWhite();
		CSoundFX*	GetPlayer();

		bool ProcessEndGame();
		void RenderDotTo(int iX, int iY);
		void RenderEnergizerTo(int iX, int iY);

	protected:

		CBomberBoy			*m_pBomberBoyBlack;
		CBomberBoy			*m_pBomberBoyWhite;
		CBackGroundPlane	*m_pBackGroundPlane; 
		CBackGroundPlane	*m_pMiddleGroundPlane; 
		CWarpDriveVFX		*m_pWarpDriveVFX;
		CWarpDriveVFX       *m_pWarpDriveVFX2;
		CFractalVFX			*m_pFractalVFX;
		CMaze				*m_pMaze; 
		CMaze				*m_pMazeBigPill;
		CMaze				*m_pMazeSmallPill;
		CActorData			*ghostData;
		CActorData			*pacmanData;
		CModel				*m_pModel;
		CPosition			*ghostPosition;
		CPosition			*pacmanPosition;
		CPacman				*m_pPacman;
		CGhost				*m_pGhostRed;
		CGhost				*m_pGhostPink;
		CGhost				*m_pGhostCyan;
		CGhost				*m_pGhostOrange;


		int			m_iWidth;
		int			m_iHeight;
		float		m_timePassedSinceStartOfGame;
};
