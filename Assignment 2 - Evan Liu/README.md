## Model class

### Initialisation

```cpp
// In CGame::Initialise()
// Step 1: New CModel()
m_pModel = new CModel();
// Step 2: SetMapPosition()
m_pModel->SetMapPosition(10, 10);
// Step 3: ReadMap(DEFAULT_MAP);  
m_pModel->ReadMap(DEFAULT_MAP);
```

### Process

```cpp
// In CGame::Process(float a_fDeltaTick)
model->Process(a_fDeltaTick);
```

### Render

```cpp
// Render Pacman by CActorData:
CActorData *pacmanData = m_pModel->GetPacmanData;
// Position
CPosition *pacmanPosition = pacmanData->GetPosition();
MovePacmanTo(pacmanPosition.GetX(), pacmanPosition.GetY());
// Direction (DirectionUp, DirectionDown, DirectionLeft, DirectionRight)
EDirection *direction = pacmanData->GetDirection();
```
