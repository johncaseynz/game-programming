#include <iostream>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

int main(int argc, char* argv[])
{
	std::cout << "First SFML Program" << std::endl;

	sf::RenderWindow window(sf::VideoMode(800, 600), "First SFML Program");

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}
		}

		window.clear();

		sf::CircleShape circle(40, 5);
		circle.setPosition(sf::Vector2f(40, 40));
		circle.setFillColor(sf::Color::Red);

		window.draw(circle);

		window.display();
	}

	return 0;
}