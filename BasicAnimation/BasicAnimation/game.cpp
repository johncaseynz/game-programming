#include <SFML/Graphics.hpp>
#include <iostream>
#include <cmath>

int main()
{
	sf::ContextSettings context;
	context.antialiasingLevel = 10;

	// create the window
	sf::RenderWindow window(sf::VideoMode(800, 600), "SFML Basic Animation", sf::Style::Default, context);
	window.setFramerateLimit(15);

	sf::Sprite sprite;

	sprite.setPosition(sf::Vector2f(window.getSize().x / 2, window.getSize().y / 2));

	sf::Texture skeleton;
	if (!skeleton.loadFromFile("assets/skeleton.png"))
	{
		std::cout << "Error loading resource skeleton.png" << std::endl;
	}

	skeleton.setSmooth(true);

	sprite.setTexture(skeleton);
	sprite.setScale(sf::Vector2f(2.0f, 2.00f));
	sprite.setPosition(400, 300);
	sprite.setOrigin(40, 30); // put it in the center

	int height = 80;
	int width = 59;

	int frame = 8;
	int frameMax = 8;
	int frameDirection = -1;

	// run the program as long as the window is open
	while (window.isOpen())
	{
		// check all the window's events that were triggered since the last iteration of the loop
		sf::Event event;
		while (window.pollEvent(event))
		{
			// "close requested" event: we close the window
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
			{

			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
			{

			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
			{

			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
			{

			}
		}
		
		std::cout << "frame:" << frame << std::endl;

		if (frame < 0)
		{
			frame = frameMax - 1;
		}
		else if (frame == frameMax)
		{
			frame = 1;
		}

		int left = frame * width;

		sprite.setTextureRect(sf::IntRect(left, 0, width, height));

		frame = frame + frameDirection;

		// clear the window with black color
 		window.clear(sf::Color::Black);

		// draw everything here...
		window.draw(sprite);

		// end the current frame
		window.display();
	}

	return 0;
}