#include <SFML/Graphics.hpp>
#include <SFML/Window.hpp>

#include <iostream>
#include <sstream>
#include <random>
#include <list>
#include <cmath>

float SCALE = 200;
float DISTANCE = 2.0f;

const int WIDTH = 800;
const int HEIGHT = 600;

std::default_random_engine generator;
std::uniform_real_distribution<float> x(-30, +30);
std::uniform_real_distribution<float> y(-30, +30);
std::uniform_real_distribution<float> z(-10, +10);

std::uniform_int_distribution<int> letters(32, 126);

void processEvents(sf::RenderWindow &window)
{
	sf::Event event;
	while (window.pollEvent(event))
	{
		if (event.type == sf::Event::Closed)
		{
			window.close();
		}
	}
}

float cartesianXToScreenX(float cartesianX, float width)
{
	return cartesianX + (width / 2.0f);
}

float cartesianYToScreenY(float cartesianY, float height)
{
	return (height / 2) - cartesianY;
}

float project(float world_point, float z)
{
	return world_point / (z + DISTANCE);
}

sf::Vector2f convert_point(sf::Vector3f point)
{
	float x = project(point.x, point.z) * SCALE;
	float y = project(point.y, point.z) * SCALE;

	float translateX = cartesianXToScreenX(x, WIDTH);
	float translateY = cartesianYToScreenY(y, HEIGHT);

	sf::Vector2f pnt;
	pnt.x = translateX;
	pnt.y = translateY;

	return pnt;
}

struct LETTER
{
	sf::Vector3f point;
	sf::Text text;
};

int main()
{
	sf::Font font;
	if (!font.loadFromFile("times.ttf"))
	{
		std::cout << "Error could not load times.ttf" << std::endl;
		return 0;
	}

	std::list <LETTER> points;


	for (int i = 0; i < 200; i++)
	{
		LETTER data;

		data.point.x = x(generator);
		data.point.y = y(generator);
		data.point.z = z(generator);

		data.text.setFont(font);
		
		data.text.setString((char)letters(generator));
		data.text.setColor(sf::Color::Red);


		points.push_back(data);
	}

	sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "SFML 3D Stars");
	window.setFramerateLimit(120);

	while (window.isOpen())
	{
		processEvents(window);
		window.clear();

		for (std::list<LETTER>::iterator it = points.begin(); it != points.end(); ++it)
		{
			sf::Vector3f world_point = it->point;
			sf::Vector2f screen_point = convert_point(world_point);

			sf::Text text = it->text;

			text.setPosition(screen_point);

			
			float far = 80 - (40 - world_point.z);

			if (far < 1.0)
			{
				far = 1.0f;
			}

			float sz = far;

			if (sz < 0.0f)
			{
				sz = 1.0f;
			}

			text.setCharacterSize(sz);

			text.setColor(sf::Color::Green);

			window.draw(text);

			it->point.z = world_point.z - 0.5f;

			if (world_point.z < -40)
			{
				it->point.x = x(generator);
				it->point.y = y(generator);
				it->point.z = z(generator);
			}
		}
		// DISTANCE = DISTANCE - 0.5f;
		window.display();
	}
}

